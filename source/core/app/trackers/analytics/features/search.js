import { getTheMostPrecedentLocations } from '@emlakjet/commons/helpers/location';

import { getEstateSearchCriteria } from 'selectors/search/estate';

/* Should have used plurals but design did not let me */
const input = {
  source: '',
  page: '',
};

const actionListeners = {
  focusSearchInput(state, action) {
    input.page = action.source;

    return {
      eventCategory: 'search',
      eventAction: 'search-input-focused',
      eventLabel: `${action.source}`,
    };
  },
  showSearchFilters() {
    input.page = 'search-and-filter';

    return {
      eventCategory: 'search-and-filter',
      eventAction: 'search-and-filter-clicked',
    };
  },
  selectDropdown() {
    input.source = 'dropdown';

    return false;
  },
  selectSuggestion() {
    input.source = 'suggestion-box';

    return false;
  },
  doEstateSearch(state, action) {
    const criteria = action.criteria || getEstateSearchCriteria(state);
    const { locations } = criteria;
    const { q } = criteria.params;
    let freeTextTerms;
    let structure = '';
    let suffix = '';
    let termsToString = '';
    let eventCategory = 'search';
    let eventAction = 'search-0-term';
    let eventLabel;

    /* Get the locations with highest priority
     * if user is searching for "Kadıköy - Acıbadem", the search actually
     * means the user is searching for "Acıbadem"
     * thus, "Kadıköy" is removed
     */
    const theMostPrecedentLocations = getTheMostPrecedentLocations(locations);

    /* Extract free text terms from params
     *
     */
    if (q && q.length > 0) {
      freeTextTerms = q.split(' ');
    }

    const terms = [];

    /* Gather terms from token
     * Set search type to structured
     */
    if (theMostPrecedentLocations && theMostPrecedentLocations.length > 0) {
      theMostPrecedentLocations.forEach((location) => {
        terms.push({ name: location.name, type: location.type });
      });
      structure = 'structured';
    }

    /* Gather terms from free text
     * Set search type to free-text
     */
    if (freeTextTerms && freeTextTerms.length > 0) {
      actionListeners.selectSuggestion();
      freeTextTerms.forEach((term) => {
        terms.push({ name: term, type: 'free' });
      });
      structure = 'free-text';
    }

    const termCount = freeTextTerms
      ? theMostPrecedentLocations.length + freeTextTerms.length
      : theMostPrecedentLocations.length;

    if (termCount > 1) {
      suffix = 's';
    }

    /* Check if there are both tokens and free texts within the search parameters
     * Set search type to hybrid
     */
    if (
      theMostPrecedentLocations &&
      theMostPrecedentLocations.length > 0 &&
      freeTextTerms &&
      freeTextTerms.length > 0
    ) {
      structure = 'hybrid';
    }

    /* Create eventAction string
     * using structure-search-term(s)-locationType-locationName format
     */
    if (input.source === 'suggestion-box' && terms.length > 0) {
      terms.forEach((term) => {
        termsToString += `${term.type}-`;
      });

      terms.forEach((term) => {
        termsToString += `${term.name}-`;
      });
      termsToString = termsToString.substring(0, termsToString.length - 1);
      eventAction = `${structure}-search-${termCount}-term${suffix}-${termsToString}`;
    } else if (terms.length > 0) {
      terms.forEach((term) => {
        termsToString += `${term.type}-selected-`;
      });
      terms.forEach((term) => {
        termsToString += `${term.name}-`;
      });
      termsToString = termsToString.substring(0, termsToString.length - 1);
      eventAction = `${termCount}-${termsToString}`;
    }

    if (input.page === 'search-and-filter') {
      eventLabel = 'search-and-filter';
    } else {
      eventLabel = 'search-page-mobile';
    }

    /*
     * Create eventCategory string
     */
    if (input.source !== 'dropdown') {
      eventCategory = 'search';
    } else {
      eventCategory = 'location-filters';
    }

    return {
      eventCategory: `${eventCategory}`,
      eventAction: `${eventAction}`,
      eventLabel: `${eventLabel}`,
    };
  },
};

export default actionListeners;
