import { changeOrderType, changeSearchViewType, clickSaveSearch } from 'actions/ui/bottom-sticky-buttons';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({});

const mapDispatchToProps = {
  changeOrderType,
  changeSearchViewType,
  clickSaveSearch,
};

export default [mapStateToProps, mapDispatchToProps];
