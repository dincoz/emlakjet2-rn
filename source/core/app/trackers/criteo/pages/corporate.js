import { criteo } from 'app-config';

const actionListeners = {
  corporateLoadRouteSuccess() {
    /* User segment will be set to 2 if a corporate account is logged in */
    const events = [
      { event: 'setAccount', account: criteo.accountId },
      { event: 'setSiteType', type: 'm' },
      { event: 'viewHome', user_segment: '1' },
    ];

    return events;
  },
};

export default actionListeners;
