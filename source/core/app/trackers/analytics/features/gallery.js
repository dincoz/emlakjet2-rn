const actionListeners = {
  slideListingDetailPhoto(state, action) {
    if (action.page === 2) {
      return {
        eventCategory: 'listing-photo-slider',
        eventAction: `photo-${action.page}-clicked`,
      };
    }

    return {};
  },
  extendListingPhotoGallery(state, action) {
    if (action.page === 2) {
      return {
        eventCategory: 'listing-photo-gallery',
        eventAction: `photo-${action.page}-clicked`,
      };
    } else if (!action.page) {
      return {
        eventCategory: 'listing-photo-gallery',
        eventAction: 'photo-gallery-opened',
      };
    }

    return {};
  },
};

export default actionListeners;
