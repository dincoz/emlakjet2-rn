import storage from 'core/app/storage';

export default store => next => (action) => {
  storage.exporter(store.getState(), action);

  return next(action);
};
