import { takeLatest, put, select } from 'redux-saga/effects';
import { listings } from '@emlakjet/services/source';
import { getAccountSearchCriteria } from 'selectors/search/account';

import {
  fetchAccountListingsFailure,
  fetchAccountShowcaseSuccess,
  replaceAccountSearchCriteria,
} from 'actions/search/account';

/**
 * Import actions symbols.
 */
import { FETCH_ACCOUNT_SHOWCASE } from 'symbols/search/account';

/**
 * Watch fetch estate search action.
 */
function* watcher() {
  yield takeLatest(FETCH_ACCOUNT_SHOWCASE, function* fetchAccountShowcase({ criteria }) {
    let searchCriteria = criteria;
    let response;
    let state;

    if (!searchCriteria) {
      state = yield select();
      searchCriteria = getAccountSearchCriteria(state);
    }

    try {
      response = yield listings.search(searchCriteria);
    } catch (e) {
      return yield put(fetchAccountListingsFailure(e));
    }

    if (response.data.listing.length < 3) {
      try {
        yield put(
          replaceAccountSearchCriteria({
            params: {
              ACCOUNT_ID: searchCriteria.params.ACCOUNT_ID,
              pageSize: 10,
            },
          })
        );

        state = yield select();
        searchCriteria = getAccountSearchCriteria(state);
        response = yield listings.search(searchCriteria);
      } catch (e) {
        return yield put(fetchAccountListingsFailure(e));
      }
    }

    return yield put(fetchAccountShowcaseSuccess(response.data));
  });
}

export default watcher;
