import accountDetail from './account-detail';
import listingDetail from './listing-detail';

export default {
  accountDetail,
  detail: listingDetail,
};
