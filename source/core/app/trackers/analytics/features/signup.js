import {
  getCurrentRoutingPagename
} from 'selectors/routing';

const labels = {
  homepage: 'home-page-mobile',
  detail: 'listing-detail-mobile',
  accountDetail: 'account-listing-mobile',
  listings: 'listing-mobile'
};

const actionType = {
  k: 'corporate-user-signed-up',
  b: 'individual-user-signed-up-via-email',
  bs: 'individual-user-signed-up-via-facebook',
};

const actionError = {
  b: 'individual',
  k: 'corporate'
};

const actionListeners = {
  doSignUpSuccess(state, action) {
    const label = getCurrentRoutingPagename(state);
    const type = actionType[action.result.userType];

    return {
      eventCategory: 'sign-up',
      eventAction: `${type}`,
      eventLabel: `${labels[label]}`
    };
  },
  changeSignupTab(state, action) {
    const label = getCurrentRoutingPagename(state);
    const type = action.data.Tab;

    return {
      eventCategory: 'sign-up',
      eventAction: `${type}-signup-tab-clicked`,
      eventLabel: `${labels[label]}`
    };
  },
  clickSignUpFacebook(state) {
    const label = getCurrentRoutingPagename(state);

    return {
      eventCategory: 'sign-up',
      eventAction: 'facebook-signup-clicked',
      eventLabel: `${labels[label]}`
    };
  },
  doSignUpError(state, action) {
    const label = getCurrentRoutingPagename(state);

    return {
      eventCategory: 'sign-up',
      eventAction: `${actionError[action.data.type]}-signup-error${action.data.errors}`,
      eventLabel: `${labels[label]}`
    };
  }
};

export default actionListeners;
