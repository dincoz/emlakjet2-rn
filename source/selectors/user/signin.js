export const getLoggedInUser = state => state.user.signin.result && state.user.signin.result.user;

export const isUserSignedIn = state => !!getLoggedInUser(state);
