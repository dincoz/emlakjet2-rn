import { getRoutingHistory } from 'selectors/routing';

import { getEstateSearchLastCriteria } from 'selectors/search/estate';

export default {
  replaceEstateSearchLastCriteria(state, storageState, action) {
    getRoutingHistory(state);

    return {
      estateSearchLastCriteria: {
        path: 'searches.estate.lastCriteria',
        data: (action && action.criteria) || getEstateSearchLastCriteria(state),
        excludedPages: ['listings'],
        isImmutable: true,
      },
    };
  },
};
