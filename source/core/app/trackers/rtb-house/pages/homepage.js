const actionListeners = {
  homepageLoadRouteSuccess() {
    return ['_home'];
  },
};

export default actionListeners;
