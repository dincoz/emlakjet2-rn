import { combineReducers } from 'redux';

import { APP_TYPE_DETECTED } from 'symbols/initial';

export default combineReducers({
  appType: (appType = null, action) => {
    if (action.type === APP_TYPE_DETECTED) {
      return action.appType;
    }

    return appType;
  },
});
