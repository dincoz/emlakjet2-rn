import appConfig from 'app-config';

import { camelCaseToKebabCase } from '@emlakjet/commons/utils/string';

import getAppType from 'core/client/app-type-detector';
import { getCurrentRouting, getRoutingHistory } from 'selectors/routing';
import { LOAD_ROUTE_SUCCESS } from 'symbols/routing';

import pages from './pages';
import features from './features';

const actionListeners = {
  ...pages,
  ...features,
};

const shortPageNames = {
  homepage: 'HP',
  detail: 'DP',
  listings: 'LP',
  accountDetail: 'ADP',
  404: '404',
  500: '500',
  accountSearch: 'ASP',
  contact: 'CCP',
  corporate: 'CP',
  about: 'ABP',
};

const eventSender = ReactGA => (actionListenerName, isPageView, state, action) => {
  const actionListener = actionListeners[actionListenerName];

  if (!isPageView && !actionListener) {
    return;
  }

  let title;

  if (isPageView && action && action.seoData) {
    title = action.seoData.title;
  }

  const currentRouting = getCurrentRouting(state);

  const location =
    window.location.origin + (currentRouting.pathname ? currentRouting.pathname : '');
  const customEventParameters = actionListener ? actionListener(state, action) : {};
  const routingHistory = getRoutingHistory(state);

  const pageName = `${camelCaseToKebabCase(
    currentRouting.pagename === 'detail' ? 'listingDetail' : currentRouting.pagename
  )}-mobile`;

  let historyString;

  if (isPageView) {
    historyString = routingHistory
      .reduce((acc, item) => {
        const shortPageName = shortPageNames[item.pagename];

        if (shortPageName && acc[acc.length - 1] !== shortPageName) {
          acc.push(shortPageName);
        }

        return acc;
      }, [])
      .join('>');
  }

  const sendEvent = (params) => {
    if (isPageView) {
      window.customDimensions = {};

      window.customDimensions = Object.keys(params).reduce((acc, key) => {
        if (key.indexOf('dimension') === 0) {
          acc[key] = params[key];
        }

        return acc;
      }, {});
    }

    let event = isPageView
      ? {
        hitType: 'pageView',
      }
      : {
        hitType: 'event',
        eventLabel: pageName,
        nonInteraction: true,
      };

    const { lookerToast } = state.listing;

    event = {
      ...event,
      ...window.customDimensions,
      ...params,
      location,
      dimension11: historyString || 'XXX',
      dimension16: lookerToast && lookerToast.visible ? 'on' : 'off',
    };

    if (title) {
      event.title = title;
    }

    if (action.type === LOAD_ROUTE_SUCCESS && routingHistory.length >= 2) {
      const previousHistoryItem = routingHistory[routingHistory.length - 2];
      const ga = ReactGA.ga();

      ga('set', 'referrer', window.location.origin + (previousHistoryItem.pathname || ''));
    }

    ReactGA.ga('send', event);
  };

  if (customEventParameters && Array.isArray(customEventParameters)) {
    customEventParameters.forEach(sendEvent);
  } else if (customEventParameters) {
    sendEvent(customEventParameters);
  }
};

export default () => {
  // eslint-disable-next-line
  const ReactGA = require('react-ga');
  const appType = getAppType();

  ReactGA.initialize(appConfig.keys.google.analytics[appType]);
  ReactGA.plugin.require('displayfeatures');
  ReactGA.plugin.require('linkid');

  return eventSender(ReactGA);
};
