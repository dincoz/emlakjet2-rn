import { takeLatest, put } from 'redux-saga/effects';
import { favorites } from '@emlakjet/services/source';

import { createFavoriteSuccess, createFavoriteFailure } from 'actions/user/favorites';

/**
 * Import actions symbols.
 */
import { CREATE_FAVORITE } from 'symbols/user/favorites';

function* watcher() {
  yield takeLatest(CREATE_FAVORITE, function* createFavorite({ listId, listingId }) {
    try {
      yield favorites.createFavoriteById(listId, { listingId });
    } catch (e) {
      return yield put(createFavoriteFailure(e));
    }

    return yield put(createFavoriteSuccess({ listId, listingId }));
  });
}

export default watcher;
