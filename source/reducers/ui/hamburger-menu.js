import { combineReducers } from 'redux';

import { SET_HAMBURGER_MENU_VISIBILITY } from 'symbols/hamburger-menu';

export default combineReducers({
  visible: (state = false, action) => {
    switch (action.type) {
      case SET_HAMBURGER_MENU_VISIBILITY:
        return action.visible;

      default:
        return state;
    }
  },
});
