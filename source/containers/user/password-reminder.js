import { doPasswordReminder } from 'actions/user/password-reminder';

import {
  getPasswordReminderResult,
  isPasswordReminderInProgress,
} from 'selectors/user/password-reminder';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({
  passwordReminderResult: getPasswordReminderResult(state),
  inProgress: isPasswordReminderInProgress(state),
});

const mapDispatchToProps = {
  doPasswordReminder,
};

// Export mappers.
export default [mapStateToProps, mapDispatchToProps];
