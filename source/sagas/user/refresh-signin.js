import { takeLatest, put } from 'redux-saga/effects';
import { user } from '@emlakjet/services/source';
import Storage from 'source/native/Storage';
import { doSignInFailure, doSignInSuccess } from 'actions/user/signin';

import { fetchDefaultFavoriteList } from 'actions/user/favorites';

/**
 * Import actions symbols.
 */
import { DO_REFRESH_SIGN_IN } from 'symbols/user/signin';

/**
 * Watch fetch estate search action.
 */
function* watcher() {
  yield takeLatest(DO_REFRESH_SIGN_IN, function* refreshSignIn({ data }) {
    let response;

    try {
      response = yield user.checkRefreshToken(data);
    } catch (e) {
      return yield put(doSignInFailure(e));
    }

    Storage.local.set('authorization', `EJT ${response.headers.authorization}`);
    Storage.local.set('refreshToken', `EJT ${response.headers['x-refresh-token']}`);
    yield put(doSignInSuccess(response.data));

    if (response.data && response.data.id) {
      return yield put(fetchDefaultFavoriteList());
    }

    return true;
  });
}

export default watcher;
