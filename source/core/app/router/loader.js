import React, { Component } from 'react';
import { View, Dimensions } from 'react-native';
import { connect } from 'react-redux';

import { loadRoute, loadRouteResolved, loadRouteSuccess, loadRouteFailure } from 'actions/routing';

import { getHistory } from 'selectors/history';

import { getCurrentRouting, getCurrentRoutingPagename } from 'selectors/routing';


/**
 * Get page indexes.
 */
import * as HomeIndex from 'pages/home';
const IS_SERVER = true;
const APP_PLATFORM = 'native'; //(!IS_SERVER && is.mobile()) ? 'mobile' : 'desktop';

/**
 * Get pages.
 */
const Home = HomeIndex[APP_PLATFORM];

/**
 * Page components list.
 */
const mapPageToComponent = {
  homepage: Home,
};

/**
 * Route loader component.
 */
class RouteLoader extends Component {
  constructor(props) {
    super(props);

    const { currentRouting: { inProgress }, history, location } = props;
    // Prevent double construct issue on server.
    if (inProgress) {
      this.loadRoute(history.getFullPathname(location));
    }
  }

  componentWillReceiveProps(nextProps) {
    const { currentRouting } = this.props;
    const { currentRouting: nextRouting, history } = nextProps;
    const currentPathname = history.getFullPathname();

    /**
     * Create loadRoute wrapper function.
     */
    const loadRouteWrapper = () => this.loadRoute(currentPathname, history.action);

    /**
     * Is this thing going out of hand?
     * Yes
     */
    if (Date.now() - nextRouting.lastActionTime > 3000 && nextRouting.inProgress) {
      return loadRouteWrapper();
    }

    /**
     * STOP if pathname doesnt changed.
     */

    if (currentRouting.pathname === currentPathname && nextRouting.pathname === currentPathname) {
      return false;
    }

    /**
     * STOP if change triggered by hash.
     */
    if (currentPathname.includes('#')) {
      return false;
    }

    /**
     * STOP If state is not changed and routing still in progress.
     */
    if (currentRouting.state === nextRouting.state && nextRouting.inProgress) {
      return false;
    }

    /**
     * STOP If state is resolved and thing still in progress.
     */
    if (nextRouting.state === 'resolved' && nextRouting.inProgress) {
      return false;
    }

    /**
     * STOP if routing just started.
     */
    if (currentRouting.state === 'initial' && nextRouting.state === 'started') {
      return false;
    }

    /**
     * STOP if route resolved and fetching in progress.
     */
    if (currentRouting.state === 'started' && nextRouting.state === 'resolved') {
      return false;
    }

    /**
     * STOP if route finished as success or failure.
     */
    if (
      currentRouting.state === 'resolved' &&
      currentRouting.inProgress &&
      (nextRouting.state === 'success' || nextRouting.state === 'failure') &&
      !nextRouting.inProgress
    ) {
      return false;
    }

    /**
     * STOP if we just started redirecting the route.
     */

    if (
      currentRouting.state === 'success' &&
      nextRouting.state === 'started' &&
      !currentRouting.inProgress &&
      nextRouting.inProgress
    ) {
      return false;
    }

    /**
     * Load route.
     */
    loadRouteWrapper();

    return false;
  }

  /**
   * Load route and keep time of last routing.
   */
  loadRoute(pathname, action) {
    this.props.loadRoute({
      pathname,
      action,
    });
  }

  render() {
    const { props } = this;
    const { currentRouting: { pagename, seoData = {}, inProgress } } = props;

    // Prevent intermediate renders on server.
    if (IS_SERVER && inProgress) {
      return null;
    }

    // Get component.
    let PageComponent = inProgress ? mapPageToComponent.loading : mapPageToComponent[pagename];

    // Fallback to 404 if component is undefined.
    if (!PageComponent) {
      PageComponent = mapPageToComponent[404];
    }

    return (
      <View style={{ flex: 1  }}>
        <PageComponent />
      </View>
    );
  }
}

/**
 * Connect to the store.
 */
const mapStateToProps = state => ({
  currentRouting: getCurrentRouting(state),
  currentPagename: getCurrentRoutingPagename(state),
  history: getHistory(state),
});

const mapDispatchToProps = {
  loadRoute,
  loadRouteResolved,
  loadRouteSuccess,
  loadRouteFailure,
};

export default connect(mapStateToProps, mapDispatchToProps)(RouteLoader);
