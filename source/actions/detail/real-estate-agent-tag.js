import { VIEW_LISTING_DETAIL_STICKY_CONTACT_MENU } from 'symbols/detail/real-estate-agent-tag';

export const viewListingDetailStickyContactMenu = () => ({
  type: VIEW_LISTING_DETAIL_STICKY_CONTACT_MENU,
});
