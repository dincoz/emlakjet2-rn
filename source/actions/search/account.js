import {
  DO_ACCOUNT_SEARCH,
  SET_ACCOUNT_SEARCH_CRITERIA,
  REPLACE_ACCOUNT_SEARCH_CRITERIA,
  FETCH_ACCOUNT_LISTINGS,
  FETCH_ACCOUNT_SHOWCASE,
  FETCH_ACCOUNT_LISTINGS_SUCCESS,
  FETCH_ACCOUNT_SHOWCASE_SUCCESS,
  FETCH_ACCOUNT_LISTINGS_FAILURE,
} from 'symbols/search/account';

export const doAccountSearch = criteria => ({
  type: DO_ACCOUNT_SEARCH,
  criteria,
});

export const setAccountSearchCriteria = criteria => ({
  type: SET_ACCOUNT_SEARCH_CRITERIA,
  criteria,
});

export const replaceAccountSearchCriteria = criteria => ({
  type: REPLACE_ACCOUNT_SEARCH_CRITERIA,
  criteria,
});

export const fetchAccountListings = criteria => ({
  type: FETCH_ACCOUNT_LISTINGS,
  criteria,
});

export const fetchAccountShowcase = criteria => ({
  type: FETCH_ACCOUNT_SHOWCASE,
  criteria,
});

export const fetchAccountShowcaseSuccess = result => ({
  type: FETCH_ACCOUNT_SHOWCASE_SUCCESS,
  result,
});

export const fetchAccountListingsSuccess = result => ({
  type: FETCH_ACCOUNT_LISTINGS_SUCCESS,
  result,
});

export const fetchAccountListingsFailure = error => ({
  type: FETCH_ACCOUNT_LISTINGS_FAILURE,
  error,
});
