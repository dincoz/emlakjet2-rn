import { all, fork } from 'redux-saga/effects';

/**
 * Get sagas.
 */
import detail from './detail';

/**
 * Root objects.
 */
export default function* () {
  yield all([fork(detail)]);
}
