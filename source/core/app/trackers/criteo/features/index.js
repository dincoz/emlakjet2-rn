import sendMessage from './send-message';
import phone from './phone';

const actionListeners = {
  ...sendMessage,
  ...phone,
};

export default actionListeners;
