import { takeLatest, put } from 'redux-saga/effects';
import { user } from '@emlakjet/services/source';
//import toast from '@emlakjet/react-toast';

import {
  doPasswordReminderFailure,
  doPasswordReminderSuccess,
} from 'actions/user/password-reminder';

/**
 * Import actions symbols.
 */
import { DO_PASSWORD_REMINDER } from 'symbols/user/password-reminder';

/**
 * Watch fetch estate search action.
 */
function* watcher() {
  yield takeLatest(DO_PASSWORD_REMINDER, function* passwordReminder({ data }) {
    let response;

    try {
      response = yield user.passwordReminder(data);
    } catch (e) {
      /**
       * Check system error
       */
      if (!e.response || e.response.status === 550) {
        console.log('Teknik bir hata oluştu. Lütfen tekrar dene.', e);
      }

      return yield put(doPasswordReminderFailure(e));
    }

    return yield put(doPasswordReminderSuccess(response));
  });
}

export default watcher;
