/**
 * Simple selectors.
 */
export const getCurrentRouting = state => state.routing.current;
export const getCurrentRoutingPathname = state => getCurrentRouting(state).pathname;
export const getCurrentRoutingPagename = state => getCurrentRouting(state).pagename;
export const isRedirectionInProgress = state => getCurrentRouting(state).inProgress;

export const getRoutingHistory = state => state.routing.history;
