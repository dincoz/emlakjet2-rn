import Expo from 'expo';

const storage = {};

export default Storage = {
    local: {
        set : (key, value) => {
            console.log('Set to Native Store: ', key, value);
            storage.key = value;
        },
        get : (key) => {
            return storage[key] ;
        }
    }
};
