import { createStore, applyMiddleware, compose } from 'redux';
import 'regenerator-runtime/runtime';
import createSagaMiddleware from 'redux-saga';

/**
 * Get root pieces.
 */
import rootReducer from 'reducers';
import rootSaga from 'sagas';

/**
 * Get middlewares.
 */
import logger from './middlewares/logger';
import storage from './middlewares/storage';
import trackers from './middlewares/trackers';
import historyManipulator from './middlewares/history-manipulator';

/**
 * Get initial state.
 */
import getInitialState from './initial-state';

/**
 * Store configurator.
 */
const IS_CLIENT = false;
const IS_DEV_MODE = true;

const configureStore = ({ history }) => {
  // Collect middlewares.
  const sagaMiddleware = createSagaMiddleware();

  // Redux debug tool (source: https://github.com/zalmoxisus/redux-devtools-extension)
  const composeEnhancers =
    // eslint-disable-next-line
    (IS_DEV_MODE && IS_CLIENT && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

  // Middleware list.
  const middlewares = [sagaMiddleware, logger];

  // Add conditional middlewares.
  if (IS_CLIENT) {
    middlewares.push(storage);

    if (!IS_DEV_MODE) {
      middlewares.push(trackers);
      middlewares.push(historyManipulator);
    }
  }

  // Create store with reducers, middlewares, and initial data.
  const store = createStore(
    rootReducer({ history }),
    getInitialState(),
    composeEnhancers(applyMiddleware(...middlewares))
  );

  // Register sagas.
  sagaMiddleware.run(rootSaga);
  // Return created store.
  return store;
};

/**
 * Export store creator.
 */
export default configureStore;
