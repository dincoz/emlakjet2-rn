import { merge } from 'lodash';
import { constantCaseToCamelCase } from '@emlakjet/commons/utils/string';
import Storage from 'source/native/Storage';

import exporters from './exporters';

const STORAGE_KEY = 'storageState';

export default (state, action) => {
  const isInitial = !action;

  if (isInitial) {
    const initialStorageState = {};

    Object.keys(exporters).forEach((key) => {
      const data = exporters[key](state, {});
      const name = Object.keys(data)[0];

      initialStorageState[name] = data[name];
    });

    return Storage.local.set(STORAGE_KEY, initialStorageState);
  }

  // Find exporter with action name.
  const exporter = exporters[constantCaseToCamelCase(action.type)];

  if (!exporter) {
    return false;
  }

  // Get existing storage state.
  let storageState = Storage.local.get(STORAGE_KEY) || {};

  // Export data from application state.
  const exportedData = exporter(state, storageState, action);

  // Merge new application state with the storage state.
  storageState = merge(storageState, exportedData);

  // Set new storage state.
  Storage.local.set(STORAGE_KEY, storageState);

  return false;
};
