import {
  getCurrentRoutingPagename
} from 'selectors/routing';

const labels = {
  homepage: 'home-page-mobile',
  detail: 'listing-detail-mobile',
  accountDetail: 'account-listing-mobile',
  listings: 'listing-mobile',
  favoriteDetail: 'favorites-list'
};

const actionListeners = {
  changeOrderType(state, action) {
    return {
      eventCategory: 'listing',
      eventAction: `order-type-clicked-${action.name}`,
    };
  },
  changeSearchViewType(state, action) {
    return {
      eventCategory: 'listing-map-view',
      eventAction: `listing-${action.name}-view-clicked`,
    };
  },
  clickSaveSearch(state) {
    const label = getCurrentRoutingPagename(state);

    return {
      eventCategory: 'Save-search',
      eventAction: 'Save-search-button-clicked',
      eventLabel: `${labels[label]}`,
    };
  }
};

export default actionListeners;
