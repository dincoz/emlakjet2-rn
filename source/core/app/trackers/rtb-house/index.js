import appConfig from 'app-config';
import pages from './pages';
import features from './features';

const actionListeners = {
  ...pages,
  ...features,
};

export default () => (actionListenerName, isPageView, state, action) => {
  const actionListener = actionListeners[actionListenerName];

  if (!isPageView && !actionListener) {
    return;
  }

  if (actionListener) {
    const rtbUrls = actionListener(state, action);

    let rtbDiv = document.getElementById('ej_RTBHouse');

    if (!rtbDiv) {
      rtbDiv = document.createElement('div');
      rtbDiv.setAttribute('id', 'ej_RTBHouse');

      document.body.appendChild(rtbDiv);
    } else {
      rtbDiv.innerHTML = '';
    }

    rtbUrls.forEach((url) => {
      rtbDiv.innerHTML += `<iframe src="${appConfig.rtb_house
        .url}${url}" width="1" height="2" scrolling="no" frameBorder="0" style="display: none;"></iframe>`;
    });
  }
};
