import { getEstateSearchResult, getEstateSearchCriteria } from 'selectors/search/estate';

import { getAccountSearchResult, getAccountSearchCriteria } from './search/account';

/**
 * Simple selectors.
 */
export const getMutableState = state => ({
  ...state,
  searches: {
    ...state.searches,
    estate: {
      ...state.searches.estate,
      criteria: getEstateSearchCriteria(state),
      result: getEstateSearchResult(state),
    },
    account: {
      ...state.searches.account,
      criteria: getAccountSearchCriteria(state),
      result: getAccountSearchResult(state),
    },
  },
});
