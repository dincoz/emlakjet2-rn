const actionListeners = {
  doPreparedSearch(state, action) {
    return {
      eventCategory: 'quick-search',
      eventAction: `item-${action.name}-clicked`,
    };
  },
};

export default actionListeners;
