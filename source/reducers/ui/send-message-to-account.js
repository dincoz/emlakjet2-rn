import { combineReducers } from 'redux';
import { fromJS } from 'immutable';

import {
  SEND_MESSAGE_TO_ACCOUNT,
  SEND_MESSAGE_TO_ACCOUNT_SUCCESS,
  SEND_MESSAGE_TO_ACCOUNT_FAILURE,
} from 'symbols/ui/send-message-to-account';

export default combineReducers({
  messageData: (
    messageData = fromJS({
      name: null,
      phoneNumber: null,
      listingId: null,
      surname: null,
    }),
    action
  ) => {
    switch (action.type) {
      case SEND_MESSAGE_TO_ACCOUNT:
        return fromJS(action.messageData);

      default:
        return messageData;
    }
  },

  result: (
    result = fromJS({
      messages: [],
    }),
    action
  ) => {
    switch (action.type) {
      case SEND_MESSAGE_TO_ACCOUNT_SUCCESS:
        return fromJS({
          ...action.result,
        });

      default:
        return result;
    }
  },

  isMessageSentToAccount: (state = false, action) => {
    switch (action.type) {
      case SEND_MESSAGE_TO_ACCOUNT:
        return false;

      case SEND_MESSAGE_TO_ACCOUNT_SUCCESS:
        return true;

      case SEND_MESSAGE_TO_ACCOUNT_FAILURE:
        return false;

      default:
        return state;
    }
  },

  isSendMessageToAccountInProgress: (state = false, action) => {
    switch (action.type) {
      case SEND_MESSAGE_TO_ACCOUNT:
        return true;

      case SEND_MESSAGE_TO_ACCOUNT_SUCCESS:
      case SEND_MESSAGE_TO_ACCOUNT_FAILURE:
        return false;

      default:
        return state;
    }
  },
});
