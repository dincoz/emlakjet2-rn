import { combineReducers } from 'redux';
import Storage from 'source/native/Storage';
/**
 * Get symbols.
 */
import {
  DO_SIGN_IN,
  DO_SIGN_IN_SUCCESS,
  DO_SIGN_IN_FAILURE,
  DO_SIGN_OUT,
} from 'symbols/user/signin';

/**
 * Create root reducer.
 */
export default combineReducers({
  result: (state = { user: null }, action) => {
    const { resolve, reject } = action.data || {};
    const { next } = state || {};
    const { result, error } = action;

    switch (action.type) {
      case DO_SIGN_IN:
        return {
          ...state,
          next: {
            resolve,
            reject,
          },
        };

      case DO_SIGN_IN_SUCCESS:
        if (next && next.resolve) {
          next.resolve(result);
        }

        return {
          user: result,
          error: false,
        };

      case DO_SIGN_IN_FAILURE:
        if (next && next.reject) {
          next.reject(error);
        }

        return {
          user: null,
          error,
        };

      case DO_SIGN_OUT:
        Storage.local.set('refreshToken', null);

        return {
          data: null,
        };

      default:
        return state;
    }
  },
});
