import { takeLatest, put } from 'redux-saga/effects';
import { user } from '@emlakjet/services/source';
//import toast from '@emlakjet/react-toast';
//import modal from '@emlakjet/react-modal';
//import FacebookForm from 'features/user/mobile/views/facebook/form';
//import FacebookPermission from 'features/user/mobile/views/facebook/permission';
import Storage from 'source/native/Storage';

import {
  doFacebookLoginFailure,
  doFacebookLoginSuccess
} from 'actions/user/social/facebook';

import {
  doSignInSuccess
} from 'actions/user/signin';

import {
  fetchDefaultFavoriteList
} from 'actions/user/favorites';

/**
 * Import actions symbols.
 */
import {
  DO_FACEBOOK_LOGIN
} from 'symbols/user/social/facebook';

/**
 * Watch facebook login action.
 */
function* watcher() {
  yield takeLatest(DO_FACEBOOK_LOGIN, function* ({ data }) {
    let response;

    try {
      response = yield user.social.facebook(data);
    } catch (e) {
      /**
       * Check system error
       */
      if (!e.response || e.response.status === 550) {
        console.log('Teknik bir hata oluştu. Lütfen tekrar dene.');
      }

      return yield put(doFacebookLoginFailure(e));
    }

    if (response.data.status === 'SUCCESS') {
      yield put(doFacebookLoginSuccess(response.data));

      const userData = {
        id: response.data.socialUser.memberId,
        accountId: '',
        officeId: '',
        fullName: response.data.socialUser.name,
        emailAddress: response.data.socialUser.email,
        role: 'DEFAULT',
        state: 'ACTIVE'
      };

      Storage.local.set('authorization', `EJT ${response.headers.authorization}`);
      Storage.local.set('refreshToken', `EJT ${response.headers['x-refresh-token']}`);
      yield put(doSignInSuccess(userData));

      yield put(fetchDefaultFavoriteList());
    } else if (response.data.status === 'REGISTER_REQUIRED') {
      yield put(doFacebookLoginSuccess(response.data));
      //modal.show(FacebookForm, {}, 'fullpage');
    } else {
      //modal.show(FacebookPermission, {}, 'fullpage');
    }

    return true;
  });
}

export default watcher;
