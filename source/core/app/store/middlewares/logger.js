const IS_DEV_MODE = false;
export default () => next => (action) => {
  IS_DEV_MODE &&
    // eslint-disable-next-line
    console.log(
      `%c${action.type}`,
      `background: #50D4FF; color: #000; font-size:11px; font-weight:bold; padding: 2px 5px;
     border-radius:3px; text-shadow: 0 1px 0 rgba(255, 255, 255, 0.4);`,
      { action }
    );

  return next(action);
};
