import { put, select } from 'redux-saga/effects';
import getAppType from 'core/client/app-type-detector';
import Storage from 'source/native/Storage';

import {
  appInitialized,
  appTypeDetected
} from 'actions/initial';

import {
  getCurrentRouting
} from 'selectors/routing';

import {
  doRefreshSignIn
} from 'actions/user/signin';


const IS_CLIENT = true;

export default function* () {
  if (!IS_CLIENT) {
    return;
  }
  const state = yield select();
  yield put(
    appInitialized(
      getCurrentRouting(state)
    )
  );
  const token = Storage.local.get('refreshToken');
  if (token) {
    yield new Promise(((resolve) => {
      setTimeout(() => resolve(), 1000);
    }));

    yield put(doRefreshSignIn(token));
  }

  const appType = getAppType();

  if (!appType) {
    return;
  }

  yield put(appTypeDetected(appType));

}
