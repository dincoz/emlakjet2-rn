// remove locations that are already represented by smaller locations.
export const cleanUpLocations = (city, districts, townsAndLocalities, pois, projects) => {
  const cleanUpIds = townsAndLocalities.reduce((r, loc) => {
    if (loc.district) {
      r[loc.district.id] = true;
    }

    if (loc.city) {
      r[loc.city.id] = true;
    }

    if (loc.town) {
      r[loc.town.id] = true;
    }

    if (loc.parentId) {
      r[loc.parentId] = true;
    }

    if (loc.type === 'locality' && loc.related) {
      loc.related.forEach((relatedId) => {
        r[relatedId] = true;
      });
    }

    r[loc.id] = false; // it's possible for a locality/town to refer to itself.

    return r;
  }, {});

  districts.forEach((d) => {
    if (d.parentId) {
      cleanUpIds[d.parentId] = true;
    }

    if (d.city) {
      cleanUpIds[d.city.id] = true;
    }
  });

  if (city && cleanUpIds[city.id]) {
    city = null;
  }

  const locations = [
    ...(city ? [city] : []),
    ...districts,
    ...townsAndLocalities,
    ...pois,
    ...projects,
  ].filter(loc => !cleanUpIds[loc.id]);

  if (city) {
    locations.forEach((loc) => {
      if (!loc.city) {
        loc.city = [{ id: city.id, name: city.name }];
      }
    });
  }

  return locations;
};

export const cleanupLocationArray = (locations) => {
  let city;
  const districts = [];
  const townsAndLocalities = [];
  const pois = [];
  const projects = [];

  locations.forEach((loc) => {
    if (loc.type === 'city') {
      city = loc;
    } else if (loc.type === 'district') {
      districts.push(loc);
    } else if (loc.type === 'town') {
      townsAndLocalities.push(loc);
    } else if (loc.type === 'locality') {
      townsAndLocalities.push(loc);
    } else if (loc.type === 'poi') {
      pois.push(loc);
    } else if (loc.type === 'project') {
      projects.push(loc);
    }
  });

  return cleanUpLocations(city, districts, townsAndLocalities, pois, projects);
};
