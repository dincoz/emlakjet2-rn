import { revealPhoneNumber } from 'actions/ui/phone';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({});

const mapDispatchToProps = {
  revealPhoneNumber,
};

export default [mapStateToProps, mapDispatchToProps];
