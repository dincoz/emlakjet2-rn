import listingProperties from '@emlakjet/commons/lookups/listingProperties';

import { convertToSlug, constantCaseToPascalCase } from '@emlakjet/commons/utils/string';

import { collectLocationsByType } from '@emlakjet/commons/helpers/location';

import { getEstateSearchCriteria } from 'selectors/search/estate';

const actionListeners = {
  listingsLoadRouteSuccess(state) {
    const estateSearchCriteria = getEstateSearchCriteria(state);
    const locationsByType = collectLocationsByType(estateSearchCriteria.locations);

    const createLocationDimensionValue = type => (
      locationsByType[type]
        ? locationsByType[type]
          .reduce((acc, location) => {
            acc.push(`${convertToSlug(location.name)}#${location.id}`);

            return acc;
          }, [])
          .join('|')
        : 'XXX'
    );

    const createPropertyDimensionValue = (property, subProperty) => {
      const ids = estateSearchCriteria.params[property];
      const subIds = estateSearchCriteria.params[subProperty];

      if (!ids || (subProperty && !subIds)) {
        return 'XXX';
      }

      const valueGetter = `get${constantCaseToPascalCase(subProperty || property)}Values`;
      const values = subIds
        ? listingProperties[valueGetter](ids, subIds)
        : listingProperties[valueGetter](ids);

      return Array.isArray(values)
        ? values
          .reduce((acc, value) => {
            acc.push(`${value.slug}#${value.id}`);

            return acc;
          }, [])
          .join('|')
        : `${values.slug}#${values.id}`;
    };

    const towns = createLocationDimensionValue('town');
    const localities = createLocationDimensionValue('locality');
    let townsAndLocalities = [];

    if (towns !== 'XXX') {
      townsAndLocalities.push(towns);
    }

    if (localities !== 'XXX') {
      townsAndLocalities.push(localities);
    }

    townsAndLocalities = townsAndLocalities.length ? townsAndLocalities.join('|') : 'XXX';

    return {
      dimension1: createLocationDimensionValue('city'),
      dimension2: createLocationDimensionValue('district'),
      dimension3: townsAndLocalities,
      dimension4: createPropertyDimensionValue('CATEGORY'),
      dimension5: createPropertyDimensionValue('TRADE_TYPE'),
      dimension6: 'listings',
      dimension7: createPropertyDimensionValue('CATEGORY', 'SUBCATEGORY'),
      dimension8: 'XXX',
      dimension9: 'XXX',
      dimension10: 'XXX',
      dimension12: 'XXX',
      dimension13: 'XXX',
      dimension14: 'XXX',
      dimension15: 'XXX',
      dimension16: 'XXX',
    };
  },
};

export default actionListeners;
