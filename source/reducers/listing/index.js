import { combineReducers } from 'redux';

/**
 * Get reducers.
 */
import detail from './detail';
import lookerToast from './looker-toast';

/**
 * Create reducer.
 */
export default combineReducers({
  detail,
  lookerToast
});
