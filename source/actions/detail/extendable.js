import { EXTEND_LISTING_DETAIL_DESCRIPTION } from 'symbols/detail/extendable';

export const extendListingDetailDescription = () => ({
  type: EXTEND_LISTING_DETAIL_DESCRIPTION,
});
