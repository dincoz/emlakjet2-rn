import accountDetail from './account-detail';
import listingDetail from './listing-detail';
import listings from './listings';

export default {
  accountDetail,
  detail: listingDetail,
  listings,
};
