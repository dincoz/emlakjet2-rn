import { takeLatest, put } from 'redux-saga/effects';
import { favorites } from '@emlakjet/services/source';

import { deleteFavoriteSuccess, deleteFavoriteFailure } from 'actions/user/favorites';

/**
 * Import actions symbols.
 */
import { DELETE_FAVORITE } from 'symbols/user/favorites';

function* watcher() {
  yield takeLatest(DELETE_FAVORITE, function* deleteFavorite({ listId, listingId }) {
    try {
      yield favorites.deleteFavoriteById(listId, listingId);
    } catch (e) {
      return yield put(deleteFavoriteFailure(e));
    }

    return yield put(deleteFavoriteSuccess({ listId, listingId }));
  });
}

export default watcher;
