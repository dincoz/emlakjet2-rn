import { all, fork } from 'redux-saga/effects';

/**
 * Get sagas.
 */
import initial from './initial';
import routing from './routing';
import search from './search';
import listingDetail from './listing';
import ui from './ui';
import user from './user';

/**
 * Root objects.
 */
export default function* () {
  yield all([
    fork(initial),
    fork(routing),
    fork(search),
    fork(listingDetail),
    fork(ui),
    fork(user),
  ]);
}
