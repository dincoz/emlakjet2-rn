/**
 * THIS CODE WILL BE INLINED TO HTML PAGE.
 */

/**
 * Get assets loading libraries.
 */
import { addExternalJS, addExternalCSS } from '@emlakjet/add-external-js-css';

/**
 * Add origin helper.
 */
const addOrigin = filename => `${window.location.origin}/${filename}`;

/**
 * Load required assets without blocking UI rendering.
 */
SCRIPT_FILES.map(addOrigin).forEach(filename => addExternalJS(filename));
STYLE_FILES.map(addOrigin).forEach(filename => addExternalCSS(filename));
