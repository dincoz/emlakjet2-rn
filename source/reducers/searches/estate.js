import { combineReducers } from 'redux';
import { fromJS } from 'immutable';

/**
 * Get symbols.
 */
import {
  FETCH_ESTATE_LISTINGS,
  FETCH_ESTATE_LISTINGS_SUCCESS,
  FETCH_ESTATE_LISTINGS_FAILURE,
  SET_ESTATE_SEARCH_CRITERIA,
  REPLACE_ESTATE_SEARCH_CRITERIA,
  REPLACE_ESTATE_SEARCH_LAST_CRITERIA,
} from 'symbols/search/estate';

/**
 * Import common search reducer helpers.
 */
import {
  setSearchCriteria,
  replaceSearchCriteria,
  replaceSearchLastCriteria,
  fetchListingsSuccess,
} from './common';

/**
 * Create root reducer.
 */
export default combineReducers({
  /**
   * Estate search criteria reducer.
   */
  criteria: (
    // Default estate search criteria.
    criteria = fromJS({
      locations: [],
      params: {
        TRADE_TYPE: 1,
        CATEGORY: 2,
      },
    }),
    action
  ) => {
    switch (action.type) {
      // Replace all search criteria by converting given search criteria to immutable.
      case REPLACE_ESTATE_SEARCH_CRITERIA:
        return replaceSearchCriteria(criteria, action);

      // Set search criteria.
      case SET_ESTATE_SEARCH_CRITERIA:
        return setSearchCriteria(criteria, action);

      default:
        // Return state as is.
        return criteria;
    }
  },

  lastCriteria: (lastCriteria = null, action) => {
    if (action.type === REPLACE_ESTATE_SEARCH_LAST_CRITERIA) {
      return replaceSearchLastCriteria(lastCriteria, action);
    }

    return lastCriteria;
  },
  /**
   * Estate search results reducer.
   */
  result: (
    // Default empty estate search result.
    result = fromJS({
      total: 0,
      listings: [],
    }),
    action
  ) => {
    if (action.type === FETCH_ESTATE_LISTINGS_SUCCESS) {
      // Create new state from new result object.
      return fetchListingsSuccess(action);
    }

    // Return as is.
    return result;
  },

  /**
   * Search progress status reducer.
   */
  isEstateSearchInProgress: (state = false, action) => {
    switch (action.type) {
      // Set true when fetching start.
      case FETCH_ESTATE_LISTINGS:
        return true;

      // Set false when fetching succeeded or failed.
      case FETCH_ESTATE_LISTINGS_SUCCESS:
      case FETCH_ESTATE_LISTINGS_FAILURE:
        return false;

      // Return as is.
      default:
        return state;
    }
  },
});
