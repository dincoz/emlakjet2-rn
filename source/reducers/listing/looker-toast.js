
/**
 * Get symbols.
 */
import { FETCH_LISTING_DETAIL_SUCCESS } from 'symbols/listing-detail';
import { SET_LOOKER_TOAST_VISIBILITY } from 'symbols/detail/looker-toast';

/**
 * Create root reducer.
 */
export default (state = null, action) => {
  switch (action.type) {
    case FETCH_LISTING_DETAIL_SUCCESS:
      return {
        visible: false
      };

    case SET_LOOKER_TOAST_VISIBILITY:
      return {
        visible: action.isLookerToastVisible
      };

    default:
      // Return as is.
      return state;
  }
};
