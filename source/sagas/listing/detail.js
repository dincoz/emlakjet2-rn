import { takeLatest, put } from 'redux-saga/effects';
import { listings } from '@emlakjet/services/source';

import { fetchListingDetailFailure, fetchListingDetailSuccess } from 'actions/listing/detail';

/**
 * Import actions symbols.
 */
import { FETCH_LISTING_DETAIL } from 'symbols/listing-detail';

/**
 * Watch fetch estate search action.
 */
function* watcher() {
  yield takeLatest(FETCH_LISTING_DETAIL, function* fetchListingDetail({ listingId }) {
    let response;

    try {
      response = yield listings.listingDetail(listingId);
    } catch (e) {
      return yield put(fetchListingDetailFailure(e));
    }

    return yield put(fetchListingDetailSuccess(response.data));
  });
}

export default watcher;
