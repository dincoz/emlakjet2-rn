import {
  DO_FACEBOOK_LOGIN,
  DO_FACEBOOK_LOGIN_SUCCESS,
  DO_FACEBOOK_LOGIN_FAILURE,
  CLICK_LOGIN_FACEBOOK,
  OPEN_FACEBOOK_MODAL,
} from 'symbols/user/social/facebook';

export const doFacebookLogin = data => ({
  type: DO_FACEBOOK_LOGIN,
  data
});

export const doFacebookLoginFailure = error => ({
  type: DO_FACEBOOK_LOGIN_FAILURE,
  error
});

export const doFacebookLoginSuccess = result => ({
  type: DO_FACEBOOK_LOGIN_SUCCESS,
  result
});

export const clickLoginFacebook = data => ({
  type: CLICK_LOGIN_FACEBOOK,
  data
});

export const openFacebookModal = () => ({
  type: OPEN_FACEBOOK_MODAL,
});
