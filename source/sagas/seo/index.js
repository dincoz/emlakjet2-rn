import SeoService from '@emlakjet/commons/helpers/seo-service';
import appConfig from 'app-config';

function* seoAnalyzer({
  pathname, pagename, parsedURLData, initialData
}) {
  return yield SeoService.getSeoData(
    appConfig.app.hostname,
    appConfig.cdn,
    pagename,
    pathname,
    initialData,
    parsedURLData
  );
}

export default seoAnalyzer;
