/**
 * Export constants.
 */
export const SHARE_THROUGH_MODAL = 'SHARE_THROUGH_MODAL';
export const SHARE_THROUGH_WIDGET = 'SHARE_THROUGH_WIDGET';
