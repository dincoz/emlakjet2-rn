const actionListeners = {
  viewListingDetailStickyContactMenu() {
    return {
      eventCategory: 'listing-detail-sticky-agent-menu',
      eventAction: 'agent-name-clicked',
    };
  },
};

export default actionListeners;
