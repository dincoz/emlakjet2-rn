import { all, fork } from 'redux-saga/effects';

/**
 * Get sagas.
 */
import loadRoute from './load-route';
import loadRouteResolved from './load-route-resolved';

/**
 * Root objects.
 */
export default function* () {
  yield all([fork(loadRoute), fork(loadRouteResolved)]);
}
