import { selectDropdown, selectSuggestion } from 'actions/search/filter';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({});

const mapDispatchToProps = {
  selectDropdown,
  selectSuggestion,
};

export default [mapStateToProps, mapDispatchToProps];
