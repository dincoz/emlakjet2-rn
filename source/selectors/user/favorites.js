export const getFavorites = state => state.user.favorites;

export const getFavoritesDetailDefaultId = (state) => {
  const favorites = getFavorites(state);

  return favorites && favorites.lists && favorites.lists.getIn(['default', 'id']);
};

export const getFavoritesDetailCount = (state) => {
  const favorites = getFavorites(state);

  return favorites && favorites.lists && favorites.lists.get('total');
};
