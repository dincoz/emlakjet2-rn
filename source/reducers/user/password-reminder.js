import { combineReducers } from 'redux';

/**
 * Get symbols.
 */
import {
  DO_PASSWORD_REMINDER,
  DO_PASSWORD_REMINDER_SUCCESS,
  DO_PASSWORD_REMINDER_FAILURE,
} from 'symbols/user/password-reminder';

/**
 * Create root reducer.
 */
export default combineReducers({
  result: (result = false, action) => {
    if (action.type === DO_PASSWORD_REMINDER_SUCCESS) {
      return {
        error: false,
      };
    }

    if (action.type === DO_PASSWORD_REMINDER_FAILURE) {
      return {
        error: action.error.response ? action.error.response.status : true,
      };
    }

    // Return as is.
    return result;
  },

  isPasswordReminderInProgress: (state = false, action) => {
    switch (action.type) {
      case DO_PASSWORD_REMINDER:
        return {
          state: true,
          error: true,
        };

      case DO_PASSWORD_REMINDER_SUCCESS:
      case DO_PASSWORD_REMINDER_FAILURE:
        return false;

      default:
        return state;
    }
  },
});
