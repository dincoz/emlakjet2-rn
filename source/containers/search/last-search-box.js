import { doLastSearchClicked, doLastSearchEmail, doLastSearchFacebook } from 'actions/search/last-search';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({});

const mapDispatchToProps = {
  doLastSearchClicked,
  doLastSearchEmail,
  doLastSearchFacebook,
};

// Export mappers.
export default [mapStateToProps, mapDispatchToProps];
