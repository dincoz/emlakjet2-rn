export const getPasswordReminderResult = state => state.user.passwordReminder.result;
export const isPasswordReminderInProgress = state =>
  state.user.passwordReminder.isPasswordReminderInProgress;
