/**
 * Import related symbols.
 */
import { DO_ESTATE_SEARCH } from 'symbols/search/estate';

/**
 * Import routing actions.
 */
import { loadRouteResolved } from 'actions/routing';

/**
 * Import estate search actions.
 */
import { replaceEstateSearchCriteria } from 'actions/search/estate';

/**
 * Import estate search state selectors.
 */
import { getEstateSearchCriteria } from 'selectors/search/estate';

import { takeLatest, select, put } from 'redux-saga/effects';
import SearchURLMaker from '@emlakjet/commons/helpers/search-url-maker';
import shortCircuits from './short-circuits';

/**
 * Watch do estate search action.
 */
function* watcher() {
  yield takeLatest(DO_ESTATE_SEARCH, function* doEstateSearch({ criteria }) {
    let searchCriteria = criteria;

    // When no criteria passed with action, take estate search criteria from store.
    if (!searchCriteria) {
      const state = yield select();

      searchCriteria = getEstateSearchCriteria(state);
    }

    /**
     * Check short-circuits before search.
     */
    if (yield* shortCircuits(searchCriteria)) {
      return false;
    }

    // Replace all estate search criteria with active criteria.
    yield put(replaceEstateSearchCriteria(searchCriteria));

    // TODO: Persist lastSearch.

    // Push new search URL to the history.
    return yield put(
      loadRouteResolved({
        pathname: new SearchURLMaker(searchCriteria).get(),
        pagename: 'listings',
        parsedURLData: searchCriteria,
      })
    );
  });
}

export default watcher;
