import { getAppType } from 'selectors/platform/app-type';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({
  appType: getAppType(state),
});

const mapDispatchToProps = {};

export default [mapStateToProps, mapDispatchToProps];
