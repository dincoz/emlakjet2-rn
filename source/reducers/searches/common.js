import { fromJS } from 'immutable';
import normalizeListing from '@emlakjet/normalizers/listing';

/**
 * Replace search criteria.
 */
export const replaceSearchCriteria = (criteria, action) => fromJS(action.criteria);

/**
 * Replace last search criteria.
 */
export const replaceSearchLastCriteria = (criteria, action) => fromJS(action.criteria);

/**
 * Set search criteria.
 */
export const setSearchCriteria = (criteria, action) => {
  // Given params object to be merged with existing criteria.
  criteria = criteria.set('params', criteria.get('params').merge(action.criteria.params));

  // Given locations array overwrites existing locations array.
  if (action.criteria.locations) {
    return criteria.set('locations', action.criteria.locations);
  }

  // Return new state.
  return criteria;
};

/**
 * Set search result by normalizing every item.
 */
export const fetchListingsSuccess = (action) => {
  const { result, result: { listing } } = action;

  return fromJS({
    ...result,
    listing: undefined,
    listings: listing.map(normalizeListing),
  });
};
