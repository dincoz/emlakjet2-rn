import homepage from './homepage';
import listings from './listings';
import listingDetail from './listing-detail';

const actionListeners = {
  ...homepage,
  ...listings,
  ...listingDetail,
};

export default actionListeners;
