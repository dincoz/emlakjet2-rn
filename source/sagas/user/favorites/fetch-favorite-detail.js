import { takeLatest, put } from 'redux-saga/effects';
import { favorites } from '@emlakjet/services/source';

import {
  fetchFavoriteListDetailSuccess,
  fetchFavoriteListDetailFailure,
} from 'actions/user/favorites';

/**
 * Import actions symbols.
 */
import { FETCH_FAVORITE_LIST_DETAIL } from 'symbols/user/favorites';

function* watcher() {
  yield takeLatest(FETCH_FAVORITE_LIST_DETAIL, function* fetchFavoriteListDetail({ listId }) {
    let response;

    try {
      response = yield favorites.getFavoriteByIdIndexed(listId);
    } catch (e) {
      return yield put(fetchFavoriteListDetailFailure(e));
    }

    return yield put(fetchFavoriteListDetailSuccess(response.data));
  });
}

export default watcher;
