import { all, fork } from 'redux-saga/effects';

/**
 * Get sagas.
 */
import fetch from './fetch';
import doSearch from './do-search';

/**
 * Root objects.
 */
export default function* () {
  yield all([fork(fetch), fork(doSearch)]);
}
