import React, { Component } from 'react';
import { View, Text, Linking, Dimensions } from 'react-native';
import { ButtonGroup, Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import appConfig from 'app-config';

import Touchable from "components/elements/touchable/Touchable";

import searchBoxContainer from 'containers/search/box';
import filterContainer from 'containers/search/filter';
import locationSearchProvider from 'features/search/common/location-search-provider';

/**
 * Box component
 */
class SearchBoxWithTabs extends Component {

    tabTitles = ['Satılık', 'Kiralık', 'Projeler'];
    tabs = [{ tradeId: 1 }, { tradeId: 2 }];

    constructor(props) {
        super(props);
        //this.showModal = this.showModal.bind(this);
        this.doEstateSearch = props.doEstateSearch;
        this.setEstateSearchCriteria = props.setEstateSearchCriteria;
        this.onAction = this.onAction.bind(this);
        this.searchProvider = props.searchProvider;
        this.state = {
            selectedTabIndex: 0,
        };
        this.setEstateSearchCriteria({
            params: {
                TRADE_TYPE: 1,
                CATEGORY: 2,
            },
        });
    }

    onAction(queryText, tokens) {
        const { replaceEstateSearchCriteria, estateSearchCriteria } = this.props;

        replaceEstateSearchCriteria({
            locations: tokens,
            params: {
                q: queryText,
                VIEW_TYPE: undefined,
                pageSize: 30,
                TRADE_TYPE: estateSearchCriteria.params.TRADE_TYPE,
                CATEGORY: estateSearchCriteria.params.CATEGORY,
            },
        });

        this.doEstateSearch();
    }

    selectTab = (selectedTabIndex) => {
        this.setState({selectedTabIndex});

        if(selectedTabIndex === 2){
            Linking.openURL(`${appConfig.app.hostname}/projeler`);
            return;
        }

        this.props.setEstateSearchCriteria({
            params: {
                TRADE_TYPE: this.tabs[selectedTabIndex].tradeId,
            },
        });
    };

    showModal() {
        //this.props.focusSearchInput('home-page-mobile');
    }

    render() {
        return (
            <View style={styles.headerContentContainer}>
                <View style={styles.headerContentTitleContainer}>
                    <Text style={styles.headerTitleText}>Sen o evi hayal et, gerisi Emlakjet!</Text>
                </View>
                <View style={styles.headerContentButtonsContainer}>
                    <ButtonGroup
                        selectedBackgroundColor={'#52ca46'}
                        onPress={this.selectTab}
                        selectedIndex={this.state.selectedTabIndex}
                        buttons={this.tabTitles}
                        containerStyle={{ height: 40, backgroundColor: 'transparent' }}
                        buttonStyle={{ backgroundColor: 'rgba(1,1,1,0.4)' }}
                        selectedButtonStyle={{ backgroundColor: '#52ca46' }}
                        textStyle={{ color: '#fff', fontSize: common.fontSize - 5 }}
                        selectedTextStyle={{ color: '#fff' }}
                    />
                </View>
                <View style={styles.headerContentCompletionContainer}>
                    <Touchable style={styles.autoComplete} innerStyleIOS={styles.autoCompleteInnerIOS} rippleColor={'#52ca46'} onPress={this.showModal}>
                        <Text style={styles.autoCompletePlaceHolderText}>İl, ilçe, semt, okul, site, metro…</Text>
                        <Icon
                            name='search'
                            iconStyle={styles.autoCompleteSearchIcon}
                        />
                    </Touchable>
                </View>
            </View>
        );
    }
}

const common={
    debug: false,
    imageHeight: 244,
    fontSize: 20,
    fontColor: '#000000',
    fullWidth: Dimensions.get('window').width
};

const styles= {
    headerContentContainer: {
        backgroundColor: !common.debug ? 'transparent' : 'red',
        flex: 1,
    },
    headerContentTitleContainer: {
        backgroundColor: !common.debug ? 'transparent' : 'orange',
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerContentButtonsContainer: {
        backgroundColor: !common.debug ? 'transparent' : 'yellow',
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerContentCompletionContainer: {
        backgroundColor: !common.debug ? 'transparent' : 'lime',
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerImage: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        height: common.imageHeight,
        width: common.fullWidth
    },
    headerTitleText: {
        fontSize: common.fontSize,
        color: '#fff',
    },
    autoComplete: {
        backgroundColor: !common.debug ? 'white' : 'green',
        height: 40,
        borderRadius: 3,
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 8,
        flexDirection: 'row',
    },
    autoCompleteInnerIOS: {
        width: '100%',
    },
    autoCompletePlaceHolderText: {
        fontFamily: common.fontFamily,
        color: '#8d8d8d'
    },
    autoCompleteSearchIcon: {
        color: '#8d8d8d'
    }

};
/**
 * Export.
 */
export default connect(...filterContainer)(connect(...searchBoxContainer)(SearchBoxWithTabs));
