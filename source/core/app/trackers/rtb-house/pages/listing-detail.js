import { getListingDetail } from 'selectors/listing-detail';

const actionListeners = {
  detailLoadRouteSuccess(state) {
    const listing = getListingDetail(state);

    return [`_offer_${listing.id}`];
  },
};

export default actionListeners;
