import phone from './phone';

const actionListeners = {
  ...phone,
};

export default actionListeners;
