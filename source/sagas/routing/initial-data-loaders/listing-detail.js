import { take, put, race } from 'redux-saga/effects';
import helpersListing from '@emlakjet/commons/helpers/listing';

import { fetchListingDetail } from 'actions/listing/detail';

import { FETCH_LISTING_DETAIL_SUCCESS, FETCH_LISTING_DETAIL_FAILURE } from 'symbols/listing-detail';

export default function* listingDetail(parsedURLData) {
  /**
   * Dispatch initial-data fetcher action.
   */
  yield put(fetchListingDetail(parsedURLData.listingId));

  /**
   * Wait for success or failure.
   */
  const { success, failure } = yield race({
    success: take(FETCH_LISTING_DETAIL_SUCCESS),
    failure: take(FETCH_LISTING_DETAIL_FAILURE),
  });

  /**
   * On listing-detail fetch, success has to have detail.
   */
  if (success && success.detail) {
    // Also create and return pathname.
    return [success.detail, null, helpersListing.createListingURL(success.detail)];
  }

  /**
   * Return failure.
   */
  return [null, failure];
}
