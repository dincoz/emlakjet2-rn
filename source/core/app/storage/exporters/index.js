import searches from './searches';
import routing from './routing';

export default {
  ...searches,
  ...routing,
};
