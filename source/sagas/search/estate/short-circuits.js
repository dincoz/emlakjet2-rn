import { put } from 'redux-saga/effects';

/**
 * Import routing actions.
 */
import { loadRouteResolved } from 'actions/routing';

import { ACCOUNT_DETAIL } from 'core/app/router/constants';

export default function* shortCircuits(searchCriteria) {
  /**
   * Destruct criteria.
   */
  const { locations, params } = searchCriteria;

  /**
   * Check if user searched with listingId.
   */
  if (locations.length || !+params.q) {
    /**
     * There is no short-circuit.
     */
    return false;
  }

  if (params.q.length >= 7) {
    // Redirect to detail page.
    return yield put(
      loadRouteResolved({
        pathname: `ilan/ilan-detay-${params.q}`,
        pagename: 'detail',
        parsedURLData: {
          listingId: params.q,
        },
      })
    );
  }

  if (params.q.length < 7) {
    return yield put(
      loadRouteResolved({
        pathname: `${ACCOUNT_DETAIL}/emlak-ofisi-${params.q}`,
        pagename: 'accountDetail',
        parsedURLData: {
          params: {
            ACCOUNT_ID: params.q,
            ACCOUNT_LISTINGS: ACCOUNT_DETAIL,
            ACCOUNT_NAME: 'emlak-ofisi',
          },
        },
      })
    );
  }

  return true;
}
