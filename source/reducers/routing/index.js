import { combineReducers } from 'redux';

import current from './current';
import history from './history';

/**
 * Create root reducer.
 */
export default combineReducers({
  current,
  history,
});
