import {
  doFacebookLogin,
  doFacebookLoginFailure,
  clickLoginFacebook,
  openFacebookModal
} from 'actions/user/social/facebook';


import {
  getFacebookLoginResult
} from 'selectors/user/social/facebook';


const mapStateToProps = state => ({
  facebookLoginResult: getFacebookLoginResult(state),
});


const mapDispatchToProps = ({
  doFacebookLogin,
  doFacebookLoginFailure,
  clickLoginFacebook,
  openFacebookModal,
});

// Export mappers.
export default [
  mapStateToProps,
  mapDispatchToProps
];

