import { createSelector } from 'reselect';

/**
 * Get immutable estate search states.
 */
const getEstateSearchCriteriaImmutable = state => state.searches.estate.criteria;
const getEstateSearchLastCriteriaImmutable = state => state.searches.estate.lastCriteria;
const getEstateSearchResultImmutable = state => state.searches.estate.result;

/**
 * Convert immutable estate search criteria state to the pure JS object when it changed.
 */
export const getEstateSearchCriteria = createSelector(
  getEstateSearchCriteriaImmutable,
  criteria => (criteria && criteria.toJS ? criteria.toJS() : criteria)
);

/**
 * Convert immutable estate last search criteria state to the pure JS object when it changed.
 */
export const getEstateSearchLastCriteria = createSelector(
  getEstateSearchLastCriteriaImmutable,
  criteria => (criteria && criteria.toJS ? criteria.toJS() : criteria)
);

/**
 * Convert immutable estate search result state to the pure JS object when it changed.
 */
export const getEstateSearchResult = createSelector(
  getEstateSearchResultImmutable,
  result => (result && result.toJS ? result.toJS() : result)
);

export const isEstateSearchInProgress = state => state.searches.estate.isEstateSearchInProgress;
