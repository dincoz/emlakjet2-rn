import { viewListingDetailStickyContactMenu } from 'actions/detail/real-estate-agent-tag';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({});

const mapDispatchToProps = {
  viewListingDetailStickyContactMenu,
};

export default [mapStateToProps, mapDispatchToProps];
