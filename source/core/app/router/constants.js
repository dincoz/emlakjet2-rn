/**
 * Route pathname prefixes.
 */
export const LISTING_DETAIL = 'ilan';
export const ACCOUNT_DETAIL = 'emlak-ofisleri-detay';
export const CONTACT_PAGE = 'iletisim';
export const ABOUT_US_PAGE = 'hakkimizda';
export const NEWS_PAGE = 'haberler';
export const CORPORATE_PAGE = 'kurumsal';
export const HTTP_500_PAGE = '500';
export const ACCOUNT_SEARCH = 'emlak-ofisleri';
export const HTTP_404_PAGE = '404';
export const FAVORITE_DETAIL = 'favori-detay';
export const SAVED_SEARCH = 'a/hesabim/kayitli-aramalarim';
export const COOKIE_POLICY_PAGE = 'cerez-politikasi';

/**
 * Route prefix to page name map.
 */
export const mapRouteToPage = {
  [LISTING_DETAIL]: 'detail',
  [ACCOUNT_DETAIL]: 'accountDetail',
  [CONTACT_PAGE]: 'contact',
  [ABOUT_US_PAGE]: 'about',
  [NEWS_PAGE]: 'news',
  [CORPORATE_PAGE]: 'corporate',
  [HTTP_500_PAGE]: '500',
  [ACCOUNT_SEARCH]: 'accountSearch',
  [HTTP_404_PAGE]: '404',
  [FAVORITE_DETAIL]: 'favoriteDetail',
  [SAVED_SEARCH]: 'savedSearch',
  [COOKIE_POLICY_PAGE]: 'cookiePolicy'
};

