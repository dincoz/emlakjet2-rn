import { SHARE_THROUGH_MODAL, SHARE_THROUGH_WIDGET } from 'symbols/detail/share';

export const shareThroughModal = (listing, platform) => ({
  type: SHARE_THROUGH_MODAL,
  platform,
  listing,
});

export const shareThroughWidget = (listing, platform) => ({
  type: SHARE_THROUGH_WIDGET,
  platform,
  listing,
});
