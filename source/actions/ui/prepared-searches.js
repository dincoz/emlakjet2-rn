import { DO_PREPARED_SEARCH } from 'symbols/ui/prepared-searches';

export const doPreparedSearch = name => ({
  type: DO_PREPARED_SEARCH,
  name,
});
