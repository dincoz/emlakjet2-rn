import { CHANGE_ORDER_TYPE, CHANGE_SEARCH_VIEW_TYPE, CLICK_SAVE_SEARCH } from 'symbols/ui/bottom-sticky-buttons';

export const changeOrderType = name => ({
  type: CHANGE_ORDER_TYPE,
  name,
});

export const changeSearchViewType = name => ({
  type: CHANGE_SEARCH_VIEW_TYPE,
  name,
});

export const clickSaveSearch = () => ({
  type: CLICK_SAVE_SEARCH
});
