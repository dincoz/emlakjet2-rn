const mockData = {
  0: {
    id: 0,
    title: 'Tüm Şehirler',
  },
  34: {
    id: 34,
    title: 'İstanbul',
    items: [1000, 1001, 1002],
  },
  6: {
    id: 6,
    title: 'Ankara',
    items: [2000, 2001, 2002],
  },
  35: {
    id: 35,
    title: 'İzmir',
    items: [3000, 3001, 3002],
  },
  1000: {
    id: 1000,
    title: 'Tüm İstanbul',
  },
  1001: {
    id: 1001,
    title: 'Bakırköy',
  },
  1002: {
    id: 1002,
    title: 'Beylikdüzü',
  },
  2000: {
    id: 2000,
    title: 'Tüm Ankara',
  },
  2001: {
    id: 2001,
    title: 'Polatlı',
  },
  2002: {
    id: 2002,
    title: 'Batıkent',
  },
  3000: {
    id: 3000,
    title: 'Tüm İzmir',
  },
  3001: {
    id: 3001,
    title: 'Bergama',
  },
  3002: {
    id: 3002,
    title: 'Bornova',
  },
};

function dataProvider(item) {
  if (item) {
    return Promise.resolve(mockData[item.id.toString()].items.map(id => mockData[id]));
  }

  return Promise.resolve(
    Object.keys(mockData)
      .map(key => mockData[key])
      .filter(value => !!value.items || value.id === '0')
  );
}

function searchProvider(text) {
  const lowerText = text.toLowerCase();

  const result = Object.keys(mockData)
    .filter(key => mockData[key].title.toLowerCase().indexOf(lowerText) >= 0)
    .map(key => mockData[key]);

  return Promise.resolve(result);
}

export { dataProvider, searchProvider };
