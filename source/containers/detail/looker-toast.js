import { setLookerToastVisibility } from 'actions/detail/looker-toast';

const mapStateToProps = () => ({});

const mapDispatchToProps = {
  setLookerToastVisibility,
};

export default [mapStateToProps, mapDispatchToProps];
