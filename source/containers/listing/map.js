import { viewMarkerSummary } from 'actions/listing/map';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({});

const mapDispatchToProps = {
  viewMarkerSummary,
};

export default [mapStateToProps, mapDispatchToProps];
