import { SET_HAMBURGER_MENU_VISIBILITY } from 'symbols/hamburger-menu';

export const setHamburgerMenuVisibility = visible => ({
  type: SET_HAMBURGER_MENU_VISIBILITY,
  visible,
});
