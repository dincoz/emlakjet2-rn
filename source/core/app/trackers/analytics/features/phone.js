const actionListeners = {
  revealPhoneNumber(state, action) {
    const { detail } = action;
    const event = {
      eventCategory: 'phone-cloak',
      eventAction: 'reveal-phone-number',
      metric4: 1,
    };

    if (detail && detail.from && detail.from === 'gallery') {
      event.eventLabel = 'listing-detail-gallery-mobile';
    }

    return [
      event,
      {
        hitType: 'pageView',
        page: '/telefon-goruntule',
      },
    ];
  },
};

export default actionListeners;
