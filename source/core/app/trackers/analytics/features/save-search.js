const actionListeners = {
  doLastSearchClicked() {
    return {
      eventCategory: 'save-search',
      eventAction: 'save-search-button-clicked',
      eventLabel: 'listing-mobile'
    };
  },
  doLastSearchEmail() {
    return {
      eventCategory: 'save-search',
      eventAction: 'search-saved-via-email',
      eventLabel: 'listing-mobile'
    };
  },
  doLastSearchFacebook() {
    return {
      eventCategory: 'save-search',
      eventAction: 'search-saved-via-facebook',
      eventLabel: 'listing-mobile'
    };
  }
};

export default actionListeners;
