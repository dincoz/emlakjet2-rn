import React from 'react';
import { Text } from 'react-native';

import { MainApp } from 'core/app';

import createStore from 'core/app/store';
import createHistory from 'core/app/history';

/**
 * Create store and history.
 */
const history = createHistory();
const store = createStore({ history });

export default class App extends React.Component {
  render() {
      return <MainApp history={history} store={store} />
  }
}