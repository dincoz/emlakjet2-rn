import {
  VIEW_LISTING_DETAIL_MAP,
  VIEW_LISTING_DETAIL_STREET_VIEW,
  VIEW_LISTING_DETAIL_STREET_VIEW_BUTTON,
} from 'symbols/detail/map';

export const viewListingDetailMap = () => ({
  type: VIEW_LISTING_DETAIL_MAP,
});

export const viewListingDetailStreetView = () => ({
  type: VIEW_LISTING_DETAIL_STREET_VIEW,
});

export const viewListingDetailStreetViewButton = () => ({
  type: VIEW_LISTING_DETAIL_STREET_VIEW_BUTTON,
});
