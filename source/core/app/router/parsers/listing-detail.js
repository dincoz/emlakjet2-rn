import { getPathnameSection, getSectionPieces } from './_helpers';

export default pathname => ({
  listingId: getSectionPieces(getPathnameSection(pathname, -1), -1),
});
