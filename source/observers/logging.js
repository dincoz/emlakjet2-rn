import { getMutableState } from 'selectors/all';

export default ({ store$ }) => {
  /**
   * Log new state on every state update on dev mode.
   */
  const IS_DEV_MODE = true;
  if (IS_DEV_MODE) {
    store$.subscribe((state) => {
      // eslint-disable-next-line
      console.log(
        '%cSTORE UPDATED',
        `background: #6CE460; color: #000; font-size:11px; font-weight:bold;
       padding: 2px 5px; border-radius:3px; text-shadow: 0 1px 0 rgba(255, 255, 255, 0.4);`,
        { state: getMutableState(state) }
      );
    });
  }
};
