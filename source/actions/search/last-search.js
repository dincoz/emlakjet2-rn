import { DO_LAST_SEARCH_CLICKED, DO_LAST_SEARCH_EMAIL, DO_LAST_SEARCH_FACEBOOK } from 'symbols/search/last-search';

export const doLastSearchClicked = () => ({
  type: DO_LAST_SEARCH_CLICKED,
});

export const doLastSearchEmail = () => ({
  type: DO_LAST_SEARCH_EMAIL
});

export const doLastSearchFacebook = () => ({
  type: DO_LAST_SEARCH_FACEBOOK
});
