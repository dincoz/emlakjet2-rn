import { all, fork } from 'redux-saga/effects';

/**
 * Get sagas.
 */
import createFavorite from './create-favorite';
import deleteFavorite from './delete-favorite';
import fetchAllFavorite from './fetch-all-favorite';
import fetchDefaultFavorite from './fetch-default-favorite';
import fetchFavoriteDetail from './fetch-favorite-detail';

/**
 * Root objects.
 */
export default function* () {
  yield all([
    fork(createFavorite),
    fork(deleteFavorite),
    fork(fetchAllFavorite),
    fork(fetchDefaultFavorite),
    fork(fetchFavoriteDetail),
  ]);
}
