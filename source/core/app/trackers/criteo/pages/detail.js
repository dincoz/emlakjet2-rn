import { criteo } from 'app-config';

import { getListingDetail } from 'selectors/listing-detail';

const actionListeners = {
  detailLoadRouteSuccess(state, action) {
    const listing = getListingDetail(state);
    const item = action.initialData || listing;
    const events = [
      { event: 'setAccount', account: criteo.accountId },
      { event: 'setHashedEmail', email: criteo.email },
      { event: 'setSiteType', type: 'm' },
      { event: 'viewItem', item: `${item.id}` },
    ];

    return events;
  },
};

export default actionListeners;
