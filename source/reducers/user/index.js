import { combineReducers } from 'redux';

/**
 * Get reducers.
 */
import signin from './signin';
import signUp from './sign-up';
import favorites from './favorites';
import passwordReminder from './password-reminder';
import social from './social';

/**
 * Create searches reducer.
 */
export default combineReducers({
  signin,
  favorites,
  signUp,
  passwordReminder,
  social,
});
