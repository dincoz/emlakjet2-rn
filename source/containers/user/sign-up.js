import {
  doSignUp,
  changeSignupTab,
  clickSignUpFacebook,
  doSignUpError
} from 'actions/user/sign-up';

import { getSignUpResult, isRegistrationInProgress } from 'selectors/user/sign-up';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({
  signUpResult: getSignUpResult(state),
  inProgress: isRegistrationInProgress(state),
});

const mapDispatchToProps = {
  doSignUp,
  changeSignupTab,
  clickSignUpFacebook,
  doSignUpError
};

// Export mappers.
export default [mapStateToProps, mapDispatchToProps];
