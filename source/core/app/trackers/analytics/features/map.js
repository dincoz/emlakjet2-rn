const actionListeners = {
  viewListingDetailMap() {
    return {
      eventCategory: 'listing-detail-map',
      eventAction: 'map-clicked',
    };
  },
  viewListingDetailStreetView() {
    return {
      eventCategory: 'listing-detail-street-view',
      eventAction: 'street-view-clicked',
    };
  },
  viewListingDetailStreetViewButton() {
    return {
      eventCategory: 'listing-detail-street-view',
      eventAction: 'street-view-button-viewed',
    };
  },
  viewMarkerSummary(state, action) {
    return {
      eventCategory: 'listing-map-view',
      eventAction: `listing-marker-clicked-${action.listing.id}`,
    };
  },
};

export default actionListeners;
