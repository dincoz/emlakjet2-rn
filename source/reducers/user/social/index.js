import { combineReducers } from 'redux';

/**
 * Get reducers.
 */
import facebook from './facebook';

/**
 * Create searches reducer.
 */
export default combineReducers({
  facebook
});

