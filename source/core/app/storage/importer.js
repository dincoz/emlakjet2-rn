import { merge } from 'lodash';
import { fromJS } from 'immutable';
import Storage from 'source/native/Storage';

import { getCurrentRoutingPagename } from 'selectors/routing';

export default (initialState) => {
  const storageState = Storage.local.get('storageState') || {};
  const currentPageName = getCurrentRoutingPagename(initialState);
  const state = {};

  Object.keys(storageState).forEach((key) => {
    const stateItem = storageState[key];

    if (
      stateItem.expiryDate < Date.now() ||
      (Array.isArray(stateItem.excludedPages) &&
        stateItem.excludedPages.indexOf(currentPageName) > -1)
    ) {
      return;
    }

    const path = stateItem.path.split('.').reverse();

    const data = path.reduce(
      (acc, item) => ({ [item]: acc }),
      stateItem.isImmutable ? fromJS(stateItem.data) : stateItem.data
    );

    merge(state, data);
  });

  return state;
};
