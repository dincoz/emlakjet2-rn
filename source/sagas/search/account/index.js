import { all, fork } from 'redux-saga/effects';

/**
 * Get sagas.
 */
import fetch from './fetch';
import doSearch from './do-search';
import showcase from './showcase';

/**
 * Root objects.
 */
export default function* () {
  yield all([fork(fetch), fork(doSearch), fork(showcase)]);
}
