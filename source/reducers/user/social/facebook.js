import { combineReducers } from 'redux';

/**
 * Get symbols.
 */
import {
  DO_FACEBOOK_LOGIN_SUCCESS,
  DO_FACEBOOK_LOGIN_FAILURE
} from 'symbols/user/social/facebook';

/**
 * Create root reducer.
 */
export default combineReducers({

  result: (
    result = false,
    action
  ) => {
    if (action.type === DO_FACEBOOK_LOGIN_SUCCESS) {
      return {
        socialUser: action.result.socialUser,
        status: action.result.status
      };
    }

    if (action.type === DO_FACEBOOK_LOGIN_FAILURE) {
      return {
        error: action.error.response ? action.error.response.status : true
      };
    }

    // Return as is.
    return result;
  },


});
