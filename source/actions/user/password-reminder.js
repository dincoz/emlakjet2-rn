import {
  DO_PASSWORD_REMINDER,
  DO_PASSWORD_REMINDER_SUCCESS,
  DO_PASSWORD_REMINDER_FAILURE,
} from 'symbols/user/password-reminder';

export const doPasswordReminder = data => ({
  type: DO_PASSWORD_REMINDER,
  data,
});

export const doPasswordReminderFailure = error => ({
  type: DO_PASSWORD_REMINDER_FAILURE,
  error,
});

export const doPasswordReminderSuccess = result => ({
  type: DO_PASSWORD_REMINDER_SUCCESS,
  result,
});
