import { all, fork } from 'redux-saga/effects';

import signin from './signin';
import favorites from './favorites';
import signUp from './sign-up';
import passwordReminder from './password-reminder';
import refreshSignin from './refresh-signin';
import social from './social';

export default function* () {
  yield all([
    fork(signin),
    fork(favorites),
    fork(signUp),
    fork(passwordReminder),
    fork(refreshSignin),
    fork(social),
  ]);
}
