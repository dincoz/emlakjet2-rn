/**
 * Get item from list by index.
 *
 * if index other than number => list
 * positive index => [ {1}, {2}, {3} ]
 * negative => [ {-3}, {-2}, {-1} ]
 *
 * @TODO: Move this to utilz.
 */
const getFromList = (list, index) => {
  if (list) {
    return typeof index === 'number' ? list[index > -1 ? index : list.length + index] : list;
  }

  return null;
};

/**
 * Split string and clear falsy values.
 *
 * @TODO: Move this to utilz.
 */
const clearSplit = (string, splitBy) => {
  if (typeof string === 'string') {
    return string.split(splitBy).filter(Boolean);
  }

  return null;
};

/**
 * Clear query string from pathname.
 */
const clearQueryString = pathname => clearSplit(pathname, '?')[0];

/**
 * Split pathname into sections and return desired index.
 */
export const getPathnameSection = (pathname, index) =>
  getFromList(clearSplit(clearQueryString(pathname), '/'), index);

/**
 * Split section into pieces and return desired index.
 */
export const getSectionPieces = (section, index) => getFromList(clearSplit(section, '-'), index);
