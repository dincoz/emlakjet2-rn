const Server = require('http').Server;
const Express = require('express');
const universal = require('./universal');
const inliner = require('./inliner');

/**
 * Create server.
 */
const app = new Express();
const server = new Server(app);

/**
 * Universal app renderer.
 */
function renderUniversalAPP(req, res) {
  // Set headers.
  res.set({ 'content-type': 'text/html; charset=utf-8' });
  res.writeHead(200);

  /**
   * Render server side markup.
   */
  universal.renderServerSideMarkup({
    inlineScript: inliner.getInlineScript(),
    url: req.url,
    res,
  });
}

/**
 * Let the browser cache static files for forever!
 */
app.use((req, res, next) => {
  if (/\.(png|jpg|svg|css|js)$/.test(req.url)) {
    res.setHeader('Cache-Control', 'public, max-age=31556952');
    res.setHeader('Expires', new Date(Date.now() + 31556952000).toUTCString());
  }

  next();
});

/**
 * Define the folder that will be used for static assets.
 */
app.use(Express.static('build/public'));

/**
 * Compress favicon requests.
 */
app.get('/favicon.ico', (req, res) => res.send(null));

/**
 * Universal routing and rendering.
 */
app.get('*', renderUniversalAPP);

/**
 * Get port and env.
 */
const port = process.env.PORT || 3000;
const env = process.env.NODE_ENV || 'production';

/**
 * Start server.
 */
server.listen(port, (err) => {
  if (err) {
    // eslint-disable-next-line
    return console.error(err);
  }

  // Info message.
  // eslint-disable-next-line
  return console.info(`Last Commit: ${LAST_COMMIT_HASH}
    Server running on http://localhost:${port} [MODE=${env}]
  `);
});

/**
 * Log uncaught exceptions.
 */
process.on('uncaughtException', (err) => {
  // eslint-disable-next-line
  console.error('[Uncaught Exception]', err);
});
