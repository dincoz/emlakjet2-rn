/**
 * Created by Dinco-WORK on 9.07.2017.
 */
import React from 'react';
import { View, TouchableNativeFeedback, TouchableOpacity, Platform } from 'react-native';

const Touchable = (props) => {
    if(Platform.OS === 'android'){
        return (
            <TouchableNativeFeedback
                onPress={props.onPress}
                useForeground={props.useForeground}
                background={TouchableNativeFeedback.Ripple(props.rippleColor || '#ffffff', props.rippleBorder || false)}
            >
                <View pointerEvents='box-only' style={{...styles.viewContainer, ...props.style }}>
                    {props.children}
                </View>
            </TouchableNativeFeedback>
        )
    }
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={props.innerStyleIOS}
        >
            <View style={{...styles.viewContainer, ...props.style }}>
                {props.children}
            </View>
        </TouchableOpacity>
    )
};

const styles= {
    viewContainer: {
        backgroundColor: 'white',
        alignSelf: 'stretch',
    }
};

export default Touchable;