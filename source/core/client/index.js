import React from 'react';
import { render as renderReactDOM } from 'react-dom';
import { AppContainer } from 'react-hot-loader';

import { MainApp } from 'core/app';
import createStore from 'core/app/store';
import createHistory from 'core/app/history';

/**
 * Create store and history.
 */
const history = createHistory();
const store = createStore({ history });

/**
 * Initially render App.
 */
renderReactDOM(
  // AppContainer does nothing on production build.
  <AppContainer>
    <MainApp history={history} store={store} />
  </AppContainer>,
  // Root element of App.
  document.getElementById('root')
);

/**
 * Enable Hot Module Reloading.
 */
module.hot && module.hot.accept();
