import {
  DO_ESTATE_SEARCH,
  SET_ESTATE_SEARCH_CRITERIA,
  REPLACE_ESTATE_SEARCH_CRITERIA,
  REPLACE_ESTATE_SEARCH_LAST_CRITERIA,
  FETCH_ESTATE_LISTINGS,
  FETCH_ESTATE_LISTINGS_SUCCESS,
  FETCH_ESTATE_LISTINGS_FAILURE,
  FOCUS_SEARCH_INPUT,
} from 'symbols/search/estate';

export const doEstateSearch = criteria => ({
  type: DO_ESTATE_SEARCH,
  criteria,
});

export const setEstateSearchCriteria = criteria => ({
  type: SET_ESTATE_SEARCH_CRITERIA,
  criteria,
});

export const replaceEstateSearchCriteria = criteria => ({
  type: REPLACE_ESTATE_SEARCH_CRITERIA,
  criteria,
});

export const replaceEstateSearchLastCriteria = criteria => ({
  type: REPLACE_ESTATE_SEARCH_LAST_CRITERIA,
  criteria,
});

export const fetchEstateListings = criteria => ({
  type: FETCH_ESTATE_LISTINGS,
  criteria,
});

export const fetchEstateListingsSuccess = result => ({
  type: FETCH_ESTATE_LISTINGS_SUCCESS,
  result,
});

export const fetchEstateListingsFailure = error => ({
  type: FETCH_ESTATE_LISTINGS_FAILURE,
  error,
});

export const focusSearchInput = source => ({
  type: FOCUS_SEARCH_INPUT,
  source,
});
