import listings from './listings';
import listingDetail from './listing-detail';

const actionListeners = {
  ...listings,
  ...listingDetail,
};

export default actionListeners;
