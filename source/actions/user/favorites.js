import {
  CREATE_FAVORITE,
  CREATE_FAVORITE_SUCCESS,
  CREATE_FAVORITE_FAILURE,
  DELETE_FAVORITE,
  DELETE_FAVORITE_SUCCESS,
  DELETE_FAVORITE_FAILURE,
  FETCH_ALL_FAVORITE_LIST,
  FETCH_ALL_FAVORITE_LIST_SUCCESS,
  FETCH_ALL_FAVORITE_LIST_FAILURE,
  FETCH_FAVORITE_LIST_DETAIL,
  FETCH_FAVORITE_LIST_DETAIL_SUCCESS,
  FETCH_FAVORITE_LIST_DETAIL_FAILURE,
  FETCH_DEFAULT_FAVORITE_LIST,
  FETCH_DEFAULT_FAVORITE_LIST_SUCCESS,
  FETCH_DEFAULT_FAVORITE_LIST_FAILURE,
  CLICK_GO_TO_FAVORITE,
  CREATE_FAVORITE_WITHOUT_LOGIN,
  CLICK_HAMBURGER_FAVORITE_LIST,
} from 'symbols/user/favorites';

export const createFavorite = ({ listId, listingId }) => ({
  type: CREATE_FAVORITE,
  listId,
  listingId,
});

export const createFavoriteSuccess = data => ({
  type: CREATE_FAVORITE_SUCCESS,
  data,
});

export const createFavoriteFailure = error => ({
  type: CREATE_FAVORITE_FAILURE,
  error,
});

export const deleteFavorite = ({ listId, listingId }) => ({
  type: DELETE_FAVORITE,
  listId,
  listingId,
});

export const deleteFavoriteSuccess = data => ({
  type: DELETE_FAVORITE_SUCCESS,
  data,
});

export const deleteFavoriteFailure = error => ({
  type: DELETE_FAVORITE_FAILURE,
  error,
});

export const fetchFavoriteListDetail = listId => ({
  type: FETCH_FAVORITE_LIST_DETAIL,
  listId,
});

export const fetchFavoriteListDetailSuccess = data => ({
  type: FETCH_FAVORITE_LIST_DETAIL_SUCCESS,
  data,
});

export const fetchFavoriteListDetailFailure = error => ({
  type: FETCH_FAVORITE_LIST_DETAIL_FAILURE,
  error,
});

export const fetchAllFavoriteList = () => ({
  type: FETCH_ALL_FAVORITE_LIST,
});

export const fetchAllFavoriteListSuccess = data => ({
  type: FETCH_ALL_FAVORITE_LIST_SUCCESS,
  data,
});

export const fetchAllFavoriteListFailure = error => ({
  type: FETCH_ALL_FAVORITE_LIST_FAILURE,
  error,
});

export const fetchDefaultFavoriteList = () => ({
  type: FETCH_DEFAULT_FAVORITE_LIST,
});

export const fetchDefaultFavoriteListSuccess = data => ({
  type: FETCH_DEFAULT_FAVORITE_LIST_SUCCESS,
  data,
});

export const fetchDefaultFavoriteListFailure = error => ({
  type: FETCH_DEFAULT_FAVORITE_LIST_FAILURE,
  error,
});

export const clickGoToFavorite = () => ({
  type: CLICK_GO_TO_FAVORITE,
});

export const createFavoriteWithoutLogin = () => ({
  type: CREATE_FAVORITE_WITHOUT_LOGIN
});

export const clickHamburgerFavoriteList = () => ({
  type: CLICK_HAMBURGER_FAVORITE_LIST
});
