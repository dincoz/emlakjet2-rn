import { takeLatest, put } from 'redux-saga/effects';
import { user } from '@emlakjet/services/source';
//import toast from '@emlakjet/react-toast';

import { doSignUpFailure, doSignUpSuccess } from 'actions/user/sign-up';

/**
 * Import actions symbols.
 */
import { DO_SIGN_UP } from 'symbols/user/sign-up';

function* signUp({ data }) {
  let response;

  try {
    response = yield user.signUp(data);
  } catch (e) {
    /**
     * Check system error
     */
    if (!e.response || e.response.status === 550) {
      console.log('Teknik bir hata oluştu. Lütfen tekrar dene.', e);
    }

    if (e.response && e.response.status === 400) {
      let errorText;
      const { errorCode } = e.response.data;

      switch (errorCode) {
        case 101:
          errorText = 'Bu e-posta adresi veya cep telefonu bulunmaktadır.';
          break;

        case 1:
          errorText = 'Lütfen tüm alanları doldurup yeniden dene.';
          break;

        default:
          errorText = e.response.data.message;
      }

      toast.show(errorText);
    }

    return yield put(doSignUpFailure(e));
  }

  return yield put(doSignUpSuccess(response));
}

/**
 * Watch sign up actions.
 */
function* watcher() {
  yield takeLatest(DO_SIGN_UP, signUp);
}

export default watcher;
