/**
 * Export constants.
 */
export const CHANGE_ORDER_TYPE = 'CHANGE_ORDER_TYPE';
export const CHANGE_SEARCH_VIEW_TYPE = 'CHANGE_SEARCH_VIEW_TYPE';
export const CLICK_SAVE_SEARCH = 'CLICK_SAVE_SEARCH';
