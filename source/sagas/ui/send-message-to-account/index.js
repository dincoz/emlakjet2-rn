import { all, fork } from 'redux-saga/effects';

import sendMessageToAccount from './send-message-to-account';

export default function* () {
  yield all([fork(sendMessageToAccount)]);
}
