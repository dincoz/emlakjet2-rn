import { SHOW_SEARCH_FILTERS } from 'symbols/ui/listings-header';

export const showSearchFilters = () => ({
  type: SHOW_SEARCH_FILTERS,
});
