const actionListeners = {
  revealPhoneNumber(state, action) {
    const { listing } = action;

    return [`_orderstatus2_0.5_${listing.id}&amp;cd=default`];
  },
};

export default actionListeners;
