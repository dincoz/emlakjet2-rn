import { createSelector } from 'reselect';

/**
 * Get immutable estate search states.
 */
const getAccountSearchCriteriaImmutable = state => state.searches.account.criteria;
const getAccountSearchResultImmutable = state => state.searches.account.result;

/**
 * Convert immutable estate search criteria state to the pure JS object when it changed.
 */
export const getAccountSearchCriteria = createSelector(
  getAccountSearchCriteriaImmutable,
  criteria => (criteria && criteria.toJS ? criteria.toJS() : criteria)
);

/**
 * Convert immutable estate search result state to the pure JS object when it changed.
 */
export const getAccountSearchResult = createSelector(
  getAccountSearchResultImmutable,
  result => (result && result.toJS ? result.toJS() : result)
);

export const isAccountSearchInProgress = state => state.searches.account.isAccountSearchInProgress;
