import SearchURLMaker from '@emlakjet/commons/helpers/search-url-maker';

import { APP_INITIALIZED } from 'symbols/initial';

import { getListingDetail } from 'selectors/listing-detail';

import { getCurrentRouting } from 'selectors/routing';

const findRelatedListingPage = (state) => {
  const listingDetail = getListingDetail(state);
  const criteria = {
    params: {
      TRADE_TYPE: listingDetail.tradeType.id,
      CATEGORY: listingDetail.category.id,
      SUBCATEGORY: [listingDetail.subcategory.id],
    },
    locations: [
      {
        city: listingDetail.city,
        ...listingDetail.district,
        type: 'district',
      },
    ],
  };

  return new SearchURLMaker(criteria).get();
};

/**
 * Export history manipulator middleware.
 */
export default store => next => (action) => {
  /**
   * Get return value.
   */
  const returnValue = next(action);

  /**
   * Do history manipulation when app initialized.
   */
  if (action.type === APP_INITIALIZED) {
    const { history, location } = window;

    /**
     * RULES #1: Dont manipulate history if user come here from google-ads.
     */
    if (location.href.includes('gclid') || location.href.includes('utm_')) {
      return returnValue;
    }

    const state = store.getState();
    const currentRouting = getCurrentRouting(state);

    let insertHistory;

    /**
     * Determine if we need to insert history.
     */
    if (currentRouting.pagename === 'listings') {
      insertHistory = '/';
    } else if (currentRouting.pagename === 'detail') {
      insertHistory = findRelatedListingPage(state);
    }

    /**
     * Insert history.
     */
    if (insertHistory) {
      history.replaceState(null, null, insertHistory);
      history.pushState(null, null, currentRouting.pathname);
    }
  }

  /**
   * Return value.
   */
  return returnValue;
};
