import { criteo } from 'app-config';

const actionListeners = {
  sendMessageToAccount(state, action) {
    const events = [
      {
        event: 'setAccount',
        account: criteo.accountId,
      },
      {
        event: 'setHashedEmail',
        email: criteo.email,
      },
      {
        event: 'setSiteType',
        type: 'm',
      },
      {
        event: 'trackTransaction',
        id: `m_${Date.now()}`,
        item: [
          {
            id: action.messageData.listingId,
            price: '1',
            quantity: '1',
          },
        ],
      },
    ];

    return events;
  },
};

export default actionListeners;
