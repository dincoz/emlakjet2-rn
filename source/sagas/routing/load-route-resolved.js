/**
 * Import action creators.
 */
import { loadRouteSuccess, loadRouteFailure } from 'actions/routing';
import { getHistory } from 'selectors/history';

/**
 * Get symbols.
 */
import { LOAD_ROUTE_RESOLVED } from 'symbols/routing';

import { takeLatest, select, put } from 'redux-saga/effects';
import seoAnalyzer from 'sagas/seo';
import initialDataLoaders from './initial-data-loaders';

/**
 * Watch load-route-resolved action.
 */
function* watcher() {
  yield takeLatest(LOAD_ROUTE_RESOLVED, function* loadRouteResolved({
    pathname,
    pagename,
    parsedURLData,
  }) {
    /**
       * Get needed values from state.
       */
    const state = yield select();
    const history = getHistory(state);

    /**
       * When route-change request made by user, we need to update history.
       */
    if (pathname && pathname !== history.getFullPathname()) {
      history.push(pathname);
    }

    /**
       * Set pathname when initial data loaded.
       */
    const setPathname = !pathname;

    /**
       * We need to findout initialData.
       */
    let success;
    let failure;

    /**
       * Check if we need to load initial data.
       */
    if (initialDataLoaders[pagename]) {
      [success, failure, pathname = pathname] = yield* initialDataLoaders[pagename](parsedURLData);
    }

    /**
       * If something went wrong.
       */
    if (failure) {
      return yield put(
        loadRouteFailure({
          error: failure,
        })
      );
    }

    /**
       * Use success as initial-data.
       */
    const initialData = success;

    /**
       * Get seo data.
       */
    const seoData = yield* seoAnalyzer({
      pathname,
      pagename,
      parsedURLData,
      initialData,
    });

    /**
       * Check if we still in same pathname.
       */
    const isStillInSamePathname = pathname === history.getFullPathname();

    /**
       * Replace history with initial data if we are.
       */
    if (isStillInSamePathname) {
      history.replace(pathname, initialData);
    }

    /**
       * Or set pathname after initial data loaded if pathname was falsy before.
       */
    if (!isStillInSamePathname && setPathname && pathname) {
      history.push(pathname, initialData);
    }

    /**
       * Dispatch success action.
       */
    return yield put(
      loadRouteSuccess({
        initialData,
        seoData,
      })
    );
  });
}

export default watcher;
