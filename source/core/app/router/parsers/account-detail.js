import { ACCOUNT_DETAIL } from 'core/app/router/constants';
import { getPathnameSection, getSectionPieces } from './_helpers';

export default (pathname) => {
  const accountId = getSectionPieces(getPathnameSection(pathname, 1), -1);
  const page = getSectionPieces(getPathnameSection(pathname, 2), 0);
  const slug = getSectionPieces(getPathnameSection(pathname, 1)).slice(0, -1).join('-');

  return {
    locations: [],
    params: {
      ACCOUNT_ID: accountId,
      ACCOUNT_LISTINGS: ACCOUNT_DETAIL,
      ACCOUNT_NAME: slug,
      page: page ? +page : 1,
    },
  };
};
