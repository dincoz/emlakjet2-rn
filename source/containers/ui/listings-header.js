import { showSearchFilters } from 'actions/ui/listings-header';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({});

const mapDispatchToProps = {
  showSearchFilters,
};

export default [mapStateToProps, mapDispatchToProps];
