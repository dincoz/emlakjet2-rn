/**
 * Get services.
 */
import { listings } from '@emlakjet/services/source';
import parsers from './parsers';

/**
 * Import routing constants.
 */
import { mapRouteToPage } from './constants';

/**
 * Check if route exists in map.
 * Takes pathname, returns pagename or null.
 */
const matchRoutePrefix = pathname =>
  Object.keys(mapRouteToPage).reduce((resolvedPagename, pathnameInMap) => {
    // Dont go further if resolved before.
    if (resolvedPagename) {
      return resolvedPagename;
    }

    // Check for prefix.
    if (pathname.indexOf(pathnameInMap) === 1) {
      return mapRouteToPage[pathnameInMap];
    }

    return null;
  }, null);

/**
 * Promisify resolve
 */
const promisifyResolve = (pathname, pagename, parsedURLData) =>
  Promise.resolve({ pathname, pagename, parsedURLData });

/**
 * Resolve data creator.
 */
export const parseURLData = (pathname, pagename, parsedURLData) => {
  const returnResolved = promisifyResolve.bind(null, pathname, pagename);

  // If parsed url data already supplied.
  if (parsedURLData) {
    return returnResolved(parsedURLData);
  }

  // User parsers if parser of page exist.
  if (parsers[pagename]) {
    parsedURLData = parsers[pagename](pathname);
  }

  // Continue for other static url pages.
  return returnResolved(parsedURLData);
};

/**
 * Pathname resolver.
 */
export default (pathname) => {
  const returnParseURL = parseURLData.bind(null, pathname);
  const pathnameWithoutQuery = pathname.split('?')[0];

  // Check for homepage.
  if (pathnameWithoutQuery === '' || pathnameWithoutQuery === '/') {
    return returnParseURL('homepage');
  }

  // Try static resolving.
  const matched = matchRoutePrefix(pathname);

  // Look up the path in static routing map
  if (matched) {
    return returnParseURL(matched);
  }

  // Check if it is listing url.
  return listings
    .parseURL(pathname)
    .then(response => returnParseURL('listings', response.data))
    .catch(() => returnParseURL('404'));
};
