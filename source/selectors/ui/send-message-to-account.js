import { createSelector } from 'reselect';

const getMessageDataImmutable = state => state.userInteraction.sendMessageToAccount.messageData;

export const getMessageData = createSelector(
  getMessageDataImmutable,
  messageData => (messageData && messageData.toJS ? messageData.toJS() : messageData)
);

export const isSendMessageToAccountInProgress = state =>
  state.userInteraction.isSendMessageToAccountInProgress;
export const isMessageSentToAccount = state => state.userInteraction.isMessageSentToAccount;
