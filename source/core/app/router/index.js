import React from 'react';
import { Route, Switch } from 'react-router-native';
import { StaticRouter, Router } from 'react-router';

import RouteLoader from './loader';

/**
 * Determine router by global variable.
 */
const IS_CLIENT = true;
const AppRouter = IS_CLIENT ? Router : StaticRouter;

/**
 * Export routes.
 */
export default function (props) {
  // Return configured router.
  return (
    <AppRouter {...props}>
      <Switch>
        <Route exact path="*" component={RouteLoader} />
      </Switch>
    </AppRouter>
  );
}
