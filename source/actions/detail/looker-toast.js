import { SET_LOOKER_TOAST_VISIBILITY } from 'symbols/detail/looker-toast';

export const setLookerToastVisibility = isLookerToastVisible => ({
  type: SET_LOOKER_TOAST_VISIBILITY,
  isLookerToastVisible
});
