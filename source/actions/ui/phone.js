import { REVEAL_PHONE_NUMBER } from 'symbols/phone';

export const revealPhoneNumber = (listing, detail) => ({
  type: REVEAL_PHONE_NUMBER,
  listing,
  detail,
});
