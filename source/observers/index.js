import { createObservableStore } from './utils/store';

import logging from './logging';

export default (store, history) => {
  const store$ = createObservableStore(store);

  const inject = {
    history,
    store$,
    dispatch: store.dispatch,
    getState: store.getState,
  };

  logging(inject);
};
