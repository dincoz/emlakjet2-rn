/**
 * Export constants.
 */
export const APP_INITIALIZED = 'APP_INITIALIZED';

export const APP_TYPE_DETECTED = 'APP_TYPE_DETECTED';
