import { setHamburgerMenuVisibility } from 'actions/ui/hamburger-menu';

import { isHamburgerMenuVisible } from 'selectors/hamburger-menu';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({
  visible: isHamburgerMenuVisible(state),
});

const mapDispatchToProps = {
  setHamburgerMenuVisibility,
};

export default [mapStateToProps, mapDispatchToProps];
