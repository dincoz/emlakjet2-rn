import { all, fork } from 'redux-saga/effects';

/**
 * Get sagas.
 */
import facebook from './facebook';

/**
 * Root objects.
 */
export default function* () {
  yield all([
    fork(facebook)
  ]);
}

