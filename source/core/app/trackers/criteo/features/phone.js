import { criteo } from 'app-config';

const actionListeners = {
  revealPhoneNumber(state, action) {
    const events = [
      {
        event: 'setAccount',
        account: criteo.accountId,
      },
      {
        event: 'setHashedEmail',
        email: criteo.email,
      },
      {
        event: 'setSiteType',
        type: 'm',
      },
      {
        event: 'trackTransaction',
        id: `m_${Date.now()}`,
        item: [
          {
            id: action.listing.id,
            price: '1',
            quantity: '1',
          },
        ],
      },
    ];

    return events;
  },
};

export default actionListeners;
