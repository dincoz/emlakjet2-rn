import { takeLatest, put } from 'redux-saga/effects';
import { form } from '@emlakjet/services/source';

import { SEND_MESSAGE_TO_ACCOUNT } from 'symbols/ui/send-message-to-account';

import {
  sendMessageToAccountSuccess,
  sendMessageToAccountFailure,
} from 'actions/ui/send-message-to-account';

function* watcher() {
  yield takeLatest(SEND_MESSAGE_TO_ACCOUNT, function* sendMessageToAccount({ messageData }) {
    let response;

    try {
      response = JSON.parse(yield form.agencyCall(messageData));
    } catch (e) {
      return yield put(sendMessageToAccountFailure(e));
    }

    return yield put(sendMessageToAccountSuccess(response));
  });
}

export default watcher;
