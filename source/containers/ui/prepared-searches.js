import { doPreparedSearch } from 'actions/ui/prepared-searches';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({});

const mapDispatchToProps = {
  doPreparedSearch,
};

export default [mapStateToProps, mapDispatchToProps];
