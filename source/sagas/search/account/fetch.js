import { takeLatest, put, select } from 'redux-saga/effects';
import { listings, account } from '@emlakjet/services/source';

/**
 * Import actions symbols.
 */
import { FETCH_ACCOUNT_LISTINGS } from 'symbols/search/account';

import {
  replaceAccountSearchCriteria,
  fetchAccountListingsSuccess,
  fetchAccountListingsFailure,
} from 'actions/search/account';

/**
 * Import account search selectors.
 */
import { getAccountSearchCriteria } from 'selectors/search/account';

/**
 * Watch fetch account search action.
 */
function* watcher() {
  yield takeLatest(FETCH_ACCOUNT_LISTINGS, function* fetchAccountListings({ criteria }) {
    const response = {};

    if (criteria) {
      // Set given account search criteria to the state.
      yield put(replaceAccountSearchCriteria(criteria));
    }

    if (!criteria) {
      // Get all current state.
      const state = yield select();

      criteria = getAccountSearchCriteria(state);
    }

    try {
      const request = yield Promise.all([
        listings.search(criteria),
        account.details(criteria.params.ACCOUNT_ID),
      ]);

      const { 0: listingData, 1: accountData } = request;

      response.listing = listingData;
      response.account = accountData;
    } catch (e) {
      // Trigger fetch account search failed action on exception.
      return yield put(fetchAccountListingsFailure(e));
    }

    // Trigger fetch account search succeeded action.
    return yield put(fetchAccountListingsSuccess(response));
  });
}

export default watcher;
