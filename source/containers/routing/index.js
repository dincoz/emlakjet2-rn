import { loadRoute } from 'actions/routing';

import { getRoutingHistory, getCurrentRoutingPathname } from 'selectors/routing';

/**
 * Map selected state objects to the connected component's props.
 */
// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({
  currentRoutingPathname: getCurrentRoutingPathname(state),
  routingHistory: getRoutingHistory(state),
});

const mapDispatchToProps = {
  loadRoute,
};

// Export mappers.
export default [mapStateToProps, mapDispatchToProps];
