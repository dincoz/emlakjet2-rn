import { take, put, race } from 'redux-saga/effects';

import { fetchEstateListings } from 'actions/search/estate';

import {
  FETCH_ESTATE_LISTINGS_SUCCESS,
  FETCH_ESTATE_LISTINGS_FAILURE,
} from 'symbols/search/estate';

export default function* listings(parsedURLData) {
  /**
   * Dispatch initial-data fetcher action.
   */
  yield put(fetchEstateListings(parsedURLData));

  /**
   * Wait for success or failure.
   */
  const { success, failure } = yield race({
    success: take(FETCH_ESTATE_LISTINGS_SUCCESS),
    failure: take(FETCH_ESTATE_LISTINGS_FAILURE),
  });

  /**
   * On listings fetch, success has to have result.
   */
  if (success && success.result) {
    return [success.result, null];
  }

  /**
   * Return failure.
   */
  return [null, failure];
}
