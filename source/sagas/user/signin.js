import { takeLatest, put } from 'redux-saga/effects';
import { user } from '@emlakjet/services/source';
//import toast from '@emlakjet/react-toast';
import Storage from 'source/native/Storage';

import { doSignInFailure, doSignInSuccess } from 'actions/user/signin';

import { fetchDefaultFavoriteList } from 'actions/user/favorites';

/**
 * Import actions symbols.
 */
import { DO_SIGN_IN } from 'symbols/user/signin';

/**
 * Watch fetch estate search action.
 */
function* watcher() {
  yield takeLatest(DO_SIGN_IN, function* signIn({ data }) {
    let response;

    try {
      response = yield user.signin(data);
    } catch (e) {
      /**
       * Check system error
       */
      if (!e.response || e.response.status === 550) {
        console.log('Teknik bir hata oluştu. Lütfen tekrar dene.', e);
      }

      return yield put(doSignInFailure(e));
    }

    // reverse compatibility for old API
    const result = response && response.id ? response : response.data;

    Storage.local.set('authorization', `EJT ${response.headers.authorization}`);
    Storage.local.set('refreshToken', `EJT ${response.headers['x-refresh-token']}`);

    yield put(doSignInSuccess(result));

    if (result && result.id) {
      // TODO fetch all favorite lists once that feature lands
      return yield put(fetchDefaultFavoriteList());
    }

    return true;
  });
}

export default watcher;
