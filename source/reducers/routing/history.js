import { pick } from 'lodash';

/**
 * Get symbols.
 */
import {
  LOAD_ROUTE,
  LOAD_ROUTE_RESOLVED,
  LOAD_ROUTE_SUCCESS,
  LOAD_ROUTE_FAILURE
} from 'symbols/routing';

import {
  APP_INITIALIZED
} from 'symbols/initial';

/**
 * History of routing.
 */
const IS_SERVER = true;

export default (
  history = [],
  action
) => {
  if (IS_SERVER) {
    return [];
  }

  const lastEntry = history[history.length - 1] || {};
  const prevHistory = [...history.slice(0, -1)];

  switch (action.type) {
    case APP_INITIALIZED:
      return action.currentRouting ? [
        ...prevHistory,
        pick(action.currentRouting, ['pathname', 'pagename'])
      ] : history;

    case LOAD_ROUTE:
      return [
        ...(history.length ? [
          ...prevHistory,
          {
            ...lastEntry,
            scrollY: window.scrollY
          }
        ] : []),
        {
          action: action.action,
          pathname: action.pathname,
          inProgress: true
        }
      ];

    case LOAD_ROUTE_RESOLVED:
      return [
        ...prevHistory,
        {
          ...lastEntry,
          pathname: action.pathname || lastEntry.pathname,
          pagename: action.pagename,
          inProgress: true
        }
      ];

    case LOAD_ROUTE_SUCCESS:
      return [
        ...prevHistory,
        {
          ...lastEntry,
          inProgress: false
        }
      ];

    case LOAD_ROUTE_FAILURE:
      return [
        ...prevHistory,
        {
          ...lastEntry,
          error: action.error,
          inProgress: false
        }
      ];

    default:
      return history;
  }
};
