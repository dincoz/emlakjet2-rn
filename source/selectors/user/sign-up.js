export const getSignUpResult = state => state.user.signUp.result;
export const isRegistrationInProgress = state => state.user.signUp.isRegistrationInProgress;
