import { all, fork } from 'redux-saga/effects';

/**
 * Get sagas.
 */
import estate from './estate';
import account from './account';

/**
 * Root objects.
 */
export default function* () {
  yield all([fork(estate), fork(account)]);
}
