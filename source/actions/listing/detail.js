import {
  FETCH_LISTING_DETAIL,
  FETCH_LISTING_DETAIL_FAILURE,
  FETCH_LISTING_DETAIL_SUCCESS,
} from 'symbols/listing-detail';

export const fetchListingDetail = listingId => ({
  type: FETCH_LISTING_DETAIL,
  listingId,
});

export const fetchListingDetailFailure = error => ({
  type: FETCH_LISTING_DETAIL_FAILURE,
  error,
});

export const fetchListingDetailSuccess = detail => ({
  type: FETCH_LISTING_DETAIL_SUCCESS,
  detail,
});
