import { combineReducers } from 'redux';

/**
 * Get reducers.
 */
import hamburgerMenu from './hamburger-menu';

/**
 * Create searches reducer.
 */
export default combineReducers({
  hamburgerMenu,
});
