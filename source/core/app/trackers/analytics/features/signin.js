import {
  getCurrentRoutingPagename
} from 'selectors/routing';

const labels = {
  homepage: 'home-page-mobile',
  detail: 'listing-detail-mobile',
  accountDetail: 'account-listing-mobile',
  listings: 'listing-mobile',
  favoriteDetail: 'favorites-list'
};

const actionListeners = {
  doSignInSuccess(state) {
    const label = getCurrentRoutingPagename(state);

    return {
      eventCategory: 'login',
      eventAction: 'user-logged-in-via-email',
      eventLabel: `${labels[label]}`
    };
  },
  doFacebookLoginSuccess(state) {
    const label = getCurrentRoutingPagename(state);

    return {
      eventCategory: 'login',
      eventAction: 'user-logged-in-via-facebook',
      eventLabel: `${labels[label]}`
    };
  },
  openSignupModal(state) {
    const label = getCurrentRoutingPagename(state);

    return {
      eventCategory: 'login',
      eventAction: 'sign-up-clicked',
      eventLabel: `${labels[label]}`
    };
  },
  openSignInModal() {
    return {
      eventCategory: 'login',
      eventAction: 'login-modal-opened',
      eventLabel: 'hamburger-menu'
    };
  },
  openForgotPasswordModal(state) {
    const label = getCurrentRoutingPagename(state);

    return {
      eventCategory: 'login',
      eventAction: 'forgot-password-clicked',
      eventLabel: `${labels[label]}`
    };
  },
  doSignInFailure(state) {
    const label = getCurrentRoutingPagename(state);

    return {
      eventCategory: 'sign-up',
      eventAction: 'email-log-in-error',
      eventLabel: `${labels[label]}`
    };
  }


};

export default actionListeners;
