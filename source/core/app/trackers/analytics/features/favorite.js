import {
  getCurrentRoutingPagename
} from 'selectors/routing';

const labels = {
  homepage: 'home-page-mobile',
  detail: 'listing-detail-mobile',
  accountDetail: 'account-listing-mobile',
  listings: 'listing-mobile',
  favoriteDetail: 'favorites-list'
};

const actionListeners = {
  createFavorite(state) {
    const label = getCurrentRoutingPagename(state);
    const event = {
      eventCategory: 'favorites',
      eventAction: 'add-to-favorites-clicked',
      eventLabel: `${labels[label]}`
    };

    return event;
  },

  deleteFavorite(state) {
    const label = getCurrentRoutingPagename(state);
    const event = {
      eventCategory: 'favorites',
      eventAction: 'listing-deleted-from-favorites',
      eventLabel: `${labels[label]}`
    };

    return event;
  },

  clickGoToFavorite(state) {
    const label = getCurrentRoutingPagename(state);
    const event = {
      eventCategory: 'favorites',
      eventAction: 'snackbar-favorites-clicked',
      eventLabel: `${labels[label]}`
    };

    return event;
  },

  createFavoriteWithoutLogin() {
    return {
      eventCategory: 'login',
      eventAction: 'login-modal-opened',
      eventLabel: 'favorites'
    };
  },

  clickHamburgerFavoriteList(state) {
    const label = getCurrentRoutingPagename(state);

    return {
      eventCategory: 'favorites',
      eventAction: 'hamburger-favorites-clicked',
      eventLabel: `${labels[label]}`
    };
  }
};

export default actionListeners;
