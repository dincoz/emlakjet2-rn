import React from 'react';
import { renderToNodeStream, renderToStaticNodeStream } from 'react-dom/server';
import { Helmet } from 'react-helmet';
import { Readable } from 'stream';
import CircularJSON from 'circular-json';

import { App } from 'core/app';
import createStore from 'core/app/store';
import createHistory from 'core/app/history';
import HTML from 'components/pures/html';

import { isRedirectionInProgress } from 'selectors/routing';
import { getMutableState } from 'selectors/all';

/**
 * Export Server
 */
export function renderServerSideMarkup({ url, res, inlineScript = '' }) {
  const css = new Set();

  /**
   * CriticalCSS Extractor.
   */
  const context = {
    insertCss: (...styles) => {
      // eslint-disable-next-line
      styles.forEach(style => css.add(style._getCss()));
    },
  };

  /**
   * Create compounds.
   */
  const history = createHistory();
  const store = createStore({ history });

  /**
   * Create the app.
   */
  const app = <App store={store} location={url} context={context} />;

  /**
   * Subscribe to store events.
   */
  const unsubscribe = store.subscribe(() => {
    const state = store.getState();

    /**
     * If redirection flow completed.
     */
    if (!isRedirectionInProgress(state)) {
      /**
       * Unsubscribe from store.
       */
      unsubscribe();

      /**
       * Stream app rendering.
       */
      const appStream = renderToNodeStream(app);

      /**
       * Keep streamed app chunks.
       */
      const appChunks = [];

      /**
       * Collect chunks.
       */
      appStream.on('data', chunk => appChunks.push(chunk));

      /**
       * On app rendering stream completed.
       */
      appStream.on('end', () => {
        /**
         * Stringify app markup.
         */
        const markup = Buffer.concat(appChunks).toString('utf8');

        /**
         * Add inital state to inline-script.
         */
        const stringifiedState = CircularJSON.stringify(getMutableState(store.getState()));
        const finalInlineScript = `${inlineScript}; window.__INITIAL_STATE__=${stringifiedState}`;

        /**
         * Render helmet as static.
         */
        const helmet = Helmet.renderStatic();

        /**
         * Generate full page html.
         */
        const fullPageHTML = renderToStaticNodeStream(
          <HTML
            content={markup}
            inlineStyle={[...css].join('')}
            inlineScript={finalInlineScript}
            helmet={helmet}
          />
        );

        /**
         * Create stream for DOCTYPE prefix.
         */
        const doctypeStream = new Readable();
        // eslint-disable-next-line
        doctypeStream._read = function noop() {}; // Mandatory.
        doctypeStream.push('<!DOCTYPE html>');
        doctypeStream.pipe(res);

        /**
         * Stream full-page rendering as response.
         */
        fullPageHTML.pipe(res);
      });
    }
  });

  /**
   * Kick-start app.
   */
  renderToNodeStream(app).resume();
}
