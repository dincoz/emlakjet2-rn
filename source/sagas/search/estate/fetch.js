import { takeLatest, put, select } from 'redux-saga/effects';
import { listings } from '@emlakjet/services/source';

/**
 * Import actions symbols.
 */
import { FETCH_ESTATE_LISTINGS } from 'symbols/search/estate';

import {
  replaceEstateSearchCriteria,
  replaceEstateSearchLastCriteria,
  fetchEstateListingsSuccess,
  fetchEstateListingsFailure,
} from 'actions/search/estate';

/**
 * Import estate search selectors.
 */
import { getEstateSearchCriteria } from 'selectors/search/estate';

/**
 * Watch fetch estate search action.
 */
function* watcher() {
  yield takeLatest(FETCH_ESTATE_LISTINGS, function* fetchEstateListings({ criteria }) {
    let response;

    if (criteria) {
      // Set given estate search criteria to the state.
      yield put(replaceEstateSearchCriteria(criteria));
    }

    if (!criteria) {
      // Get all current state.
      const state = yield select();

      criteria = getEstateSearchCriteria(state);
    }

    try {
      response = yield listings.search(criteria);
    } catch (e) {
      // Trigger fetch estate search failed action on exception.
      return yield put(fetchEstateListingsFailure(e));
    }

    // Trigger fetch estate search succeeded action.
    yield put(fetchEstateListingsSuccess(response.data));

    // Set given estate search criteria to the prev search criteria in state.
    return yield put(replaceEstateSearchLastCriteria(criteria));
  });
}

export default watcher;
