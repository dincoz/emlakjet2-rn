/**
 * Export constants.
 */
export const FETCH_LISTING_DETAIL = 'FETCH_LISTING_DETAIL';
export const FETCH_LISTING_DETAIL_SUCCESS = 'FETCH_LISTING_DETAIL_SUCCESS';
export const FETCH_LISTING_DETAIL_FAILURE = 'FETCH_LISTING_DETAIL_FAILURE';
