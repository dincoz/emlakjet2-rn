import { combineReducers } from 'redux';
import { fromJS } from 'immutable';
import normalizeListing from '@emlakjet/normalizers/listing';

/**
 * Get symbols.
 */
import {
  CREATE_FAVORITE_SUCCESS,
  DELETE_FAVORITE_SUCCESS,
  FETCH_ALL_FAVORITE_LIST_SUCCESS,
  FETCH_DEFAULT_FAVORITE_LIST_SUCCESS,
  FETCH_FAVORITE_LIST_DETAIL_SUCCESS,
} from 'symbols/user/favorites';

import { DO_SIGN_OUT } from 'symbols/user/signin';

/**
 * Generates a map of (id -> list) tuples from a list of favorite lists. Each list in this
 * map will also be imbued with a map of (listing id -> boolean) tuples that tell whether
 * or not a listing is present on the said list.
 *
 * The resulting map additionally contains the following keys for convenience:
 *
 * `all` -> an array that contains all favorite lists
 * `default` -> the default list created for the user at registration
 * `recent` -> the most recently updated list. this changes on each favorite/unfavorite action
 * `any` -> a read-only proxy object that can be used to find out whether or not a listing is
 * favorited in any of the lists
 */
const initializeLists = function (listsData = [], defaultListId = null) {
  let defaultList = null;
  let recentList = null;

  // generate the list of favorite lists
  const lists = listsData.map((list) => {
    const listingMap = list.listings.reduce((acc, listing) => {
      const listingId = Number.isNaN(listing) ? listing.id : listing;

      acc[listingId] = true;

      return acc;
    }, {});

    const theList = {
      ...list,
      listingMap,
      createdAt: new Date(list.createdAt),
      updatedAt: list.updatedAt && new Date(list.updatedAt),
      deletedAt: list.deletedAt && new Date(list.deletedAt),
    };

    if (defaultListId && defaultListId === list.id) {
      defaultList = theList;
    }

    recentList = !recentList || recentList.createdAt <= theList.createdAt ? theList : recentList;

    return theList;
  });

  // TODO determine the default list properly if it's not specified
  defaultList = defaultList || lists[0];

  // generate the `any` proxy
  const anyProxy = lists.reduce(
    (acc, list) => {
      Object.keys(list.listingMap).forEach((listingId) => {
        acc.listingMap[listingId] = (acc.listingMap[listingId] || 0) + 1;
      });

      return acc;
    },
    { listingMap: {} }
  );

  anyProxy.listings = Object.keys(anyProxy.listingMap).map(Number);

  // generate the (id -> list) mappings
  const listsAsMap = lists.reduce((acc, list) => {
    acc[list.id] = list;

    return acc;
  }, {});

  return fromJS({
    ...listsAsMap,
    all: lists,
    default: defaultList,
    recent: recentList,
    any: anyProxy,
  });
};

/**
 * Calculate the next state based on the current state and the manner of the operation
 * that was performed.
 */
const getNextState = function (state, listId, listingId, op) {
  if (op !== 'add' && op !== 'remove') {
    return state;
  }

  // immutable js wants string keys here
  listId = `${listId}`;
  listingId = `${listingId}`;

  let result = state;

  if (op === 'remove') {
    // mark the listing as unfavorited on the list, and decrement its count by one on the `any` list
    result = result
      .deleteIn([listId, 'listingMap', listingId])
      .updateIn([listId, 'listings'], listings => listings.filter(id => id !== listingId))
      .updateIn(['any', 'listingMap', listingId], (count = 0) => Math.max(0, count - 1));
  } else {
    // mark the listing as favorited on the list, and increment its count by one on the `any` list
    result = result
      .setIn([listId, 'listingMap', listingId], true)
      .updateIn([listId, 'listings'], listings => listings.concat(listingId))
      .updateIn(['any', 'listingMap', listingId], (count = 0) => Math.min(0, count + 1));
  }

  const updatedList = result.get(listId);

  // replace the updated list
  result = result.set('all', [updatedList].concat(result.get('all').filter(x => x.id !== listId)));

  // the `default` list may actually be the list we've just updated, update its contents as well
  if (result.get('default').id === listId) {
    result = result.set('default', updatedList);
  }

  // set the updated list as `recent`
  result = result.set('recent', updatedList);

  return result;
};

/**
 * Create root reducer.
 */
export default combineReducers({
  lists: (state = null, action) => {
    const { listId, listingId } = action.data || {};

    switch (action.type) {
      case FETCH_DEFAULT_FAVORITE_LIST_SUCCESS:
      case FETCH_ALL_FAVORITE_LIST_SUCCESS:
        return initializeLists([].concat(action.data));

      case DELETE_FAVORITE_SUCCESS:
        return getNextState(state, listId, listingId, 'remove');

      case CREATE_FAVORITE_SUCCESS:
        return getNextState(state, listId, listingId, 'add');

      case DO_SIGN_OUT:
        return null;

      // eslint-disable-next-line
      case FETCH_FAVORITE_LIST_DETAIL_SUCCESS: {
        const listings = action.data.listings
          .filter(t => !t.isDeleted)
          .map(t => normalizeListing(t.detail));

        const updated = (updatedState, id) =>
          updatedState
            .updateIn([id, 'listings'], () => listings)
            .updateIn([id, 'total'], () => listings.length);

        let newState = state;

        // update all references to the specified list to contain listing details
        // rather than listing IDs
        newState = [action.data.id, 'recent', 'default'].reduce((next, name) => {
          const lId = next.getIn([name, 'id']);

          if (lId === action.data.id) {
            next = updated(next, name);
          }

          return next;
        }, newState);

        return newState;
      }

      default:
        return state;
    }
  },
});
