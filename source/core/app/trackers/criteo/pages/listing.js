import { criteo } from 'app-config';
import { getEstateSearchResult } from 'selectors/search/estate';

const actionListeners = {
  listingsLoadRouteSuccess(state) {
    const { listings } = getEstateSearchResult(state);

    const events = [
      { event: 'setAccount', account: criteo.accountId },
      { event: 'setHashedEmail', email: criteo.email },
      { event: 'setSiteType', type: 'm' },
      { event: 'viewList', item: listings ? listings.slice(0, 3).map(listing => listing.id) : [] },
    ];

    return events;
  },
};

export default actionListeners;
