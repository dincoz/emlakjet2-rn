import {
  getCurrentRoutingPagename
} from 'selectors/routing';

const labels = {
  homepage: 'home-page-mobile',
  detail: 'listing-detail-mobile',
  accountDetail: 'account-listing-mobile',
  listings: 'listing-mobile',
  favoriteDetail: 'favorites-list'
};

const actionListeners = {
  doFacebookLoginFailure(state) {
    const label = getCurrentRoutingPagename(state);

    return {
      eventCategory: 'log-in',
      eventAction: 'facebook-log-in-error',
      eventLabel: `${labels[label]}`
    };
  },
  clickLoginFacebook(state) {
    const label = getCurrentRoutingPagename(state);

    return {
      eventCategory: 'log-in',
      eventAction: 'facebook-log-in-clicked',
      eventLabel: `${labels[label]}`
    };
  },
  openFacebookModal(state) {
    const label = getCurrentRoutingPagename(state);

    return {
      eventCategory: 'log-in',
      eventAction: 'facebook-modal-opened',
      eventLabel: `${labels[label]}`
    };
  },

};

export default actionListeners;
