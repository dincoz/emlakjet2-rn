import { isUserSignedIn } from 'selectors/user/signin';

import {
  createFavorite,
  deleteFavorite,
  fetchFavoriteListDetail,
  clickGoToFavorite,
  createFavoriteWithoutLogin,
  clickHamburgerFavoriteList
} from 'actions/user/favorites';

import {
  getFavorites,
  getFavoritesDetailCount,
  getFavoritesDetailDefaultId,
} from 'selectors/user/favorites';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({
  favorites: getFavorites(state),
  favoritesDetailCount: getFavoritesDetailCount(state),
  favoritesDetailDefaultId: getFavoritesDetailDefaultId(state),
  isSignedIn: isUserSignedIn(state),
});

const mapDispatchToProps = {
  doFavorite: createFavorite,
  doUnfavorite: deleteFavorite,
  fetchFavoriteListDetail,
  clickGoToFavorite,
  createFavoriteWithoutLogin,
  clickHamburgerFavoriteList,
};

// Export mappers.
export default [mapStateToProps, mapDispatchToProps];
