import { merge } from 'lodash';
import { fromJS } from 'immutable';
import storage from 'core/app/storage';

const IS_CLIENT = true;

export default () => {
  // Check environment or corruption.
  if (!IS_CLIENT || typeof window === 'undefined') {
    return undefined;
  }

  // Get initial state.
  // eslint-disable-next-line
  const initialState = window.__INITIAL_STATE__;

  // Abort if undefined.
  if (!initialState) {
    return undefined;
  }

  const storageState = storage.importer(initialState);

  // Map initial state to immutable state when necessary.
  let state = {
    ...initialState,
    searches: {
      ...initialState.searches,
      account: {
        ...initialState.searches.account,
        criteria: fromJS(initialState.searches.account.criteria),
        result: fromJS(initialState.searches.account.result),
      },
      estate: {
        ...initialState.searches.estate,
        criteria: fromJS(initialState.searches.estate.criteria),
        lastCriteria: fromJS(initialState.searches.estate.lastCriteria),
        result: fromJS(initialState.searches.estate.result),
      },
    },
  };

  // Merge state with local storage state.
  state = merge(state, storageState);

  // Run exporters initially.
  storage.exporter(state, false);

  return state;
};
