import {
  DO_SIGN_IN,
  DO_SIGN_IN_SUCCESS,
  DO_SIGN_IN_FAILURE,
  DO_SIGN_OUT,
  DO_REFRESH_SIGN_IN,
  OPEN_SIGNUP_MODAL,
  OPEN_FORGOT_PASSWORD_MODAL,
  OPEN_SIGN_IN_MODAL,
} from 'symbols/user/signin';

export const doSignIn = data => ({
  type: DO_SIGN_IN,
  data,
});

export const doSignInFailure = error => ({
  type: DO_SIGN_IN_FAILURE,
  error,
});

export const doSignInSuccess = result => ({
  type: DO_SIGN_IN_SUCCESS,
  result,
});

export const doSignOut = () => ({
  type: DO_SIGN_OUT,
});

export const doRefreshSignIn = data => ({
  type: DO_REFRESH_SIGN_IN,
  data,
});

export const openSignupModal = () => ({
  type: OPEN_SIGNUP_MODAL,
});

export const openForgotPasswordModal = () => ({
  type: OPEN_FORGOT_PASSWORD_MODAL,
});

export const openSignInModal = () => ({
  type: OPEN_SIGN_IN_MODAL
});
