import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';

import configurePieces from 'core/config';
import observers from 'observers';
import Router from './router';

/**
 * Core app context types.
 */
const ContextType = {};
const IS_SERVER = false;

/**
 * Enables critical path CSS rendering.
 */
if (IS_SERVER) {
  ContextType.insertCss = PropTypes.func;
}

/**
 * Core app component.
 */
export class MainApp extends Component {
  constructor(props) {
    super(props);

    observers(props.store, props.history);

    configurePieces(props.store, props.history);
  }

  getChildContext() {
    return this.props.context;
  }

  render() {
    return (
      <Provider store={this.props.store}>
        <Router {...this.props} />
      </Provider>
    );
  }
}

MainApp.childContextTypes = ContextType;
