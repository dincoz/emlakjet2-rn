import get from 'lodash/get';

/**
 * Get symbols.
 */
import {
  LOAD_ROUTE,
  LOAD_ROUTE_RESOLVED,
  LOAD_ROUTE_SUCCESS,
  LOAD_ROUTE_FAILURE,
} from 'symbols/routing';

/**
 * Current routing state.
 */
export default (
  routing = {
    pagename: 'loading',
    inProgress: true,
    state: 'initial',
  },
  action
) => {
  switch (action.type) {
    case LOAD_ROUTE:
      return {
        action: action.action,
        pathname: action.pathname,
        pagename: 'loading',
        inProgress: true,
        state: 'started',
        lastActionTime: Date.now(),
      };

    case LOAD_ROUTE_RESOLVED:
      return {
        ...routing,
        pathname: action.pathname || routing.pathname,
        pagename: action.pagename,
        parsedURLData: action.parsedURLData,
        inProgress: true,
        state: 'resolved',
        lastActionTime: Date.now(),
      };

    case LOAD_ROUTE_SUCCESS:
      return {
        ...routing,
        pathname: action.pathname || routing.pathname,
        pagename: action.pagename || routing.pagename,
        parsedURLData: action.parsedURLData || routing.parsedURLData,
        initialData: action.initialData,
        seoData: action.seoData,
        inProgress: false,
        state: 'success',
        lastActionTime: Infinity,
      };

    case LOAD_ROUTE_FAILURE:
      return {
        ...routing,
        pagename: get(action, 'error.error.response.status') || '500',
        error: action.error,
        inProgress: false,
        state: 'failure',
      };

    default:
      return routing;
  }
};
