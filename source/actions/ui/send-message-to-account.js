import {
  SEND_MESSAGE_TO_ACCOUNT,
  SEND_MESSAGE_TO_ACCOUNT_SUCCESS,
  SEND_MESSAGE_TO_ACCOUNT_FAILURE,
  MESSAGE_FORM_OPENED_FROM_STICKY_MENU,
  MESSAGE_FORM_OPENED_FROM_AGENT_SECTION,
} from 'symbols/ui/send-message-to-account';

export const sendMessageToAccount = messageData => ({
  type: SEND_MESSAGE_TO_ACCOUNT,
  messageData,
});

export const sendMessageToAccountSuccess = result => ({
  type: SEND_MESSAGE_TO_ACCOUNT_SUCCESS,
  result,
});

export const sendMessageToAccountFailure = error => ({
  type: SEND_MESSAGE_TO_ACCOUNT_FAILURE,
  error,
});

export const messageFormOpenedFromStickyMenu = () => ({
  type: MESSAGE_FORM_OPENED_FROM_STICKY_MENU,
});

export const messageFormOpenedFromAgentSection = () => ({
  type: MESSAGE_FORM_OPENED_FROM_AGENT_SECTION,
});
