import React from 'react';
import { View, ScrollView, Text, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux';

import { mobile as featureSearchMobile } from 'features/search';
import searchBoxContainer from 'containers/search/box';

const homeImage = require('../../../../assets/images/home.jpg');

/**
 * Get view components for mobile.
 */
const {
    SearchBoxWithTabs,
} = featureSearchMobile;
/**
 * Home component
 */
const Home = ({
  estateSearchCriteria,
  estateSearchLastCriteria,
  setEstateSearchCriteria,
  doEstateSearch,
}) => (
    <ScrollView style={styles.container}>
        <View style={styles.imageContainer}>
            <Image resizeMode='stretch' style={styles.headerImage} source={homeImage}/>
            <SearchBoxWithTabs />
        </View>
        <View>
        </View>
        <ModalExample />
    </ScrollView>
);


const common={
    debug: false,
    imageHeight: 244,
    fullWidth: Dimensions.get('window').width
};

const styles= {
    container: {
        flex: 1
    },
    imageContainer: {
        height: common.imageHeight,
        backgroundColor: !common.debug ? 'transparent' : 'green',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 30,
        paddingBottom: 30,
    },
    headerImage: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        height: common.imageHeight,
        width: common.fullWidth
    },
};

/**
 * Export.
 */
export default Home;

