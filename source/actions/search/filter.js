import { SELECT_DROPDOWN, SELECT_SUGGESTION } from 'symbols/search/filter';

export const selectDropdown = () => ({
  type: SELECT_DROPDOWN,
});

export const selectSuggestion = () => ({
  type: SELECT_SUGGESTION,
});
