import { takeLatest, put } from 'redux-saga/effects';
import { favorites } from '@emlakjet/services/source';

import {
  fetchDefaultFavoriteListSuccess,
  fetchDefaultFavoriteListFailure,
} from 'actions/user/favorites';

/**
 * Import actions symbols.
 */
import { FETCH_DEFAULT_FAVORITE_LIST } from 'symbols/user/favorites';

function* watcher() {
  yield takeLatest(FETCH_DEFAULT_FAVORITE_LIST, function* fetchDefaultFavoriteList() {
    let response;

    try {
      response = yield favorites.getAllFavorites();
    } catch (e) {
      return yield put(fetchDefaultFavoriteListFailure(e));
    }

    // TODO call the actual service method
    return yield put(fetchDefaultFavoriteListSuccess(response.data && response.data[0]));
  });
}

export default watcher;
