const actionListeners = {
  shareThroughModal(state, action) {
    const eventAction =
      action.platform !== 'link-copied' ? `${action.platform}-clicked` : `${action.platform}`;

    return {
      eventCategory: 'sharing-modal',
      eventAction: `${eventAction}`,
    };
  },
  shareThroughWidget(state, action) {
    const eventAction =
      action.platform !== 'link-copied' ? `${action.platform}-clicked` : `${action.platform}`;

    return {
      eventCategory: 'sharing-widget',
      eventAction: `${eventAction}`,
    };
  },
};

export default actionListeners;
