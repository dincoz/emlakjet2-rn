import {
  APP_INITIALIZED,
  APP_TYPE_DETECTED
} from 'symbols/initial';

export const appInitialized = currentRouting => ({
  type: APP_INITIALIZED,
  currentRouting
});

export const appTypeDetected = appType => ({
  type: APP_TYPE_DETECTED,
  appType
});
