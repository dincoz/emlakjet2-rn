import { combineReducers } from 'redux';
import { fromJS } from 'immutable';
import normalizeListing from '@emlakjet/normalizers/listing';

/**
 * Get symbols.
 */
import {
  FETCH_ACCOUNT_LISTINGS,
  FETCH_ACCOUNT_LISTINGS_SUCCESS,
  FETCH_ACCOUNT_SHOWCASE_SUCCESS,
  FETCH_ACCOUNT_LISTINGS_FAILURE,
  SET_ACCOUNT_SEARCH_CRITERIA,
  REPLACE_ACCOUNT_SEARCH_CRITERIA,
} from 'symbols/search/account';

/**
 * Import common search reducer helpers.
 */
import { setSearchCriteria, replaceSearchCriteria } from './common';

/**
 * Create root reducer.
 */
export default combineReducers({
  /**
   * Account search criteria reducer.
   */
  criteria: (
    // Default account search criteria.
    criteria = fromJS({
      locations: [],
      params: {},
    }),
    action
  ) => {
    switch (action.type) {
      // Replace all search criteria by converting given search criteria to immutable.
      case REPLACE_ACCOUNT_SEARCH_CRITERIA:
        return replaceSearchCriteria(criteria, action);

      // Set search criteria.
      case SET_ACCOUNT_SEARCH_CRITERIA:
        return setSearchCriteria(criteria, action);

      default:
        // Return state as is.
        return criteria;
    }
  },

  /**
   * Account search results reducer.
   */
  result: (
    // Default empty account search result.
    result = fromJS({
      detail: {},
      total: 0,
      listings: [],
    }),
    action
  ) => {
    if (action.type === FETCH_ACCOUNT_LISTINGS_SUCCESS) {
      // Create new state from new result object.
      const { result: { account, listing } } = action;

      return fromJS({
        detail: account.data,
        total: listing.data.total,
        listings: listing.data.listing.map(normalizeListing),
      });
    } else if (action.type === FETCH_ACCOUNT_SHOWCASE_SUCCESS) {
      // Create new state from new result object.
      const { result: { listing } } = action;

      return fromJS({
        total: listing.total,
        listings: listing.map(normalizeListing),
      });
    }

    // Return as is.
    return result;
  },

  /**
   * Search progress status reducer.
   */
  isAccountSearchInProgress: (state = false, action) => {
    switch (action.type) {
      // Set true when fetching start.
      case FETCH_ACCOUNT_LISTINGS:
        return true;

      // Set false when fetching succeeded or failed.
      case FETCH_ACCOUNT_LISTINGS_SUCCESS:
      case FETCH_ACCOUNT_LISTINGS_FAILURE:
        return false;

      // Return as is.
      default:
        return state;
    }
  },
});
