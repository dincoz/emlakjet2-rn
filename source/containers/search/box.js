/**
 * Import estate search actions.
 */
import {
  fetchEstateListings,
  setEstateSearchCriteria,
  replaceEstateSearchCriteria,
  doEstateSearch,
  focusSearchInput,
} from 'actions/search/estate';

/**
 * Import estate search selectors.
 */
import {
  getEstateSearchResult,
  getEstateSearchCriteria,
  getEstateSearchLastCriteria,
  isEstateSearchInProgress,
} from 'selectors/search/estate';

/**
 * Map selected state objects to the connected component's props.
 */
// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({
  estateSearchResult: getEstateSearchResult(state),
  estateSearchCriteria: getEstateSearchCriteria(state),
  estateSearchLastCriteria: getEstateSearchLastCriteria(state),
  isEstateSearchInProgress: isEstateSearchInProgress(state),
});

/**
 * Map action list to the connected component's props by dispatching.
 */
const mapDispatchToProps = {
  fetchEstateListings,
  setEstateSearchCriteria,
  replaceEstateSearchCriteria,
  doEstateSearch,
  focusSearchInput,
};

// Export mappers.
export default [mapStateToProps, mapDispatchToProps];
