import { combineReducers } from 'redux';

/**
 * Get reducers.
 */
import routing from './routing';
import ui from './ui';
import searches from './searches';
import listing from './listing';
import user from './user';
import platform from './platform';

/**
 * The root reducer.
 */
const rootReducer = combineReducers({
  history: history => history || null,
  routing,
  searches,
  listing,
  ui,
  user,
  platform,
});

/**
 * Create root reducer.
 */
export default ({ history }) => (state, action) => {
  // Extend API.
  history.getFullPathname = (location = history.location) =>
    location.pathname + location.search + location.hash;

  return {
    ...rootReducer(state, action),
    history,
  };
};
