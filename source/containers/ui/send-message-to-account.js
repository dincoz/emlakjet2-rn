import {
  sendMessageToAccount,
  messageFormOpenedFromStickyMenu,
  messageFormOpenedFromAgentSection,
} from 'actions/ui/send-message-to-account';

import {
  isSendMessageToAccountInProgress,
  isMessageSentToAccount,
} from 'selectors/ui/send-message-to-account';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({
  inProgress: isSendMessageToAccountInProgress,
  isSent: isMessageSentToAccount,
});

const mapDispatchToProps = {
  sendMessageToAccount,
  messageFormOpenedFromStickyMenu,
  messageFormOpenedFromAgentSection,
};

export default [mapStateToProps, mapDispatchToProps];
