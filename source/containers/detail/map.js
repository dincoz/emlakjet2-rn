import {
  viewListingDetailMap,
  viewListingDetailStreetView,
  viewListingDetailStreetViewButton,
} from 'actions/detail/map';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({});

const mapDispatchToProps = {
  viewListingDetailMap,
  viewListingDetailStreetView,
  viewListingDetailStreetViewButton,
};

export default [mapStateToProps, mapDispatchToProps];
