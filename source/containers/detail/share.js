import { shareThroughModal, shareThroughWidget } from 'actions/detail/share';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({});

const mapDispatchToProps = {
  shareThroughModal,
  shareThroughWidget,
};

export default [mapStateToProps, mapDispatchToProps];
