import SearchUrlMaker from '@emlakjet/commons/helpers/search-url-maker';

import { getEstateSearchResult, getEstateSearchCriteria } from 'selectors/search/estate';

const actionListeners = {
  listingsLoadRouteSuccess(state) {
    const rtbUrls = [];

    const estateSearchCriteria = getEstateSearchCriteria(state);

    const categoryUrl = SearchUrlMaker.createCategoryUrl(
      Object.assign({}, estateSearchCriteria.params)
    );

    const rtbCategoryUrl = `_category2_${categoryUrl}`;

    rtbUrls.push(rtbCategoryUrl);

    const result = getEstateSearchResult(state);

    const ids = result.listings.slice(0, 5).map(l => l.id).join(',');

    if (ids.length) {
      const rtbListingsUrl = `_listing_${ids}`;

      rtbUrls.push(rtbListingsUrl);
    }

    return rtbUrls;
  },
};

export default actionListeners;
