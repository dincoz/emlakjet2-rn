import {
  doSignIn,
  doSignOut,
  openSignupModal,
  openForgotPasswordModal,
  doSignInFailure,
  openSignInModal
} from 'actions/user/signin';

import { getLoggedInUser, isUserSignedIn } from 'selectors/user/signin';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({
  currentUser: getLoggedInUser(state),
  isSignedIn: isUserSignedIn(state),
});

const mapDispatchToProps = {
  doSignIn,
  doSignOut,
  openSignupModal,
  openForgotPasswordModal,
  doSignInFailure,
  openSignInModal
};

// Export mappers.
export default [mapStateToProps, mapDispatchToProps];
