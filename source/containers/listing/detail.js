import { fetchListingDetail } from 'actions/listing/detail';

import { getListingDetail } from 'selectors/listing-detail';

/**
 * Map selected state objects to the connected component's props.
 */
// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({
  listingDetail: getListingDetail(state),
});

/**
 * Map action list to the connected component's props by dispatching.
 */
const mapDispatchToProps = {
  fetchListingDetail,
};

// Export mappers.
export default [mapStateToProps, mapDispatchToProps];
