import { takeLatest, select, put } from 'redux-saga/effects';

import routeResolver from 'core/app/router/resolver';

/**
 * Import routing actions.
 */
import { loadRouteResolved, loadRouteFailure } from 'actions/routing';

import { getHistory } from 'selectors/history';

/**
 * Get symbols.
 */
import { LOAD_ROUTE } from 'symbols/routing';

/**
 * Watch load-route action.
 */
function* watcher() {
  yield takeLatest(LOAD_ROUTE, function* loadRoute({ pathname }) {
    // Get all current state.
    const state = yield select();
    const history = getHistory(state);

    if (pathname !== history.getFullPathname()) {
      history.push(pathname);
    }

    let resolved;

    try {
      resolved = yield routeResolver(pathname);
    } catch (error) {
      // Something went wrong while resolving route.
      return yield put(
        loadRouteFailure({
          error,
        })
      );
    }

    // Resolving route successful.
    return yield put(loadRouteResolved(resolved));
  });
}

export default watcher;
