import { convertToSlug, toLowerCase } from '@emlakjet/commons/utils/string';

import { getListingDetail } from 'selectors/listing-detail';

const actionListeners = {
  detailLoadRouteSuccess(state) {
    const listing = getListingDetail(state);
    const user = listing.user;

    return {
      dimension1: `${convertToSlug(listing.city.name)}#${listing.city.id}`,
      dimension2: `${convertToSlug(listing.district.name)}#${listing.district.id}`,
      dimension3: `${convertToSlug(listing.town.name)}#${listing.town.id}`,
      dimension4: `${listing.category.slug}#${listing.category.id}`,
      dimension5: `${listing.tradeType.slug}#${listing.tradeType.id}`,
      dimension6: 'listing-detail',
      dimension7: `${listing.subcategory.slug}#${listing.subcategory.id}`,
      dimension8: user.account_name
        ? `${toLowerCase(user.account_name)}#${user.account_id}`
        : 'XXX',
      dimension9: user.office_id ? user.office_id.toString() : 'XXX',
      dimension10: 'XXX',
      dimension12: user.type === 'corporate' ? 'k' : 'b',
      dimension13: listing.id.toString(),
      dimension14: listing.price.value.toFixed(2),
      dimension23: listing.roomCount && listing.roomCount.name ? listing.roomCount.name : 'XXX',
    };
  },
};

export default actionListeners;
