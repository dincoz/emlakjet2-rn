import { extendListingDetailDescription } from 'actions/detail/extendable';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({});

const mapDispatchToProps = {
  extendListingDetailDescription,
};

export default [mapStateToProps, mapDispatchToProps];
