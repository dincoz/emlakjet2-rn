import { takeLatest, put } from 'redux-saga/effects';
import { favorites } from '@emlakjet/services/source';

import { fetchAllFavoriteListSuccess, fetchAllFavoriteListFailure } from 'actions/user/favorites';

/**
 * Import actions symbols.
 */
import { FETCH_ALL_FAVORITE_LIST } from 'symbols/user/favorites';

function* watcher() {
  yield takeLatest(FETCH_ALL_FAVORITE_LIST, function* fetchAllFavoriteList() {
    let response;

    try {
      response = yield favorites.getAllFavorites();
    } catch (e) {
      return yield put(fetchAllFavoriteListFailure(e));
    }

    return yield put(fetchAllFavoriteListSuccess(response.data));
  });
}

export default watcher;
