import home from './home';
import detail from './detail';
import listing from './listing';
import corporate from './corporate';

const actionListeners = {
  ...home,
  ...detail,
  ...listing,
  ...corporate,
};

export default actionListeners;
