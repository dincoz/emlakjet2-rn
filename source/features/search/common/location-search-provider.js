import { search } from '@emlakjet/services';

const searchHeaders = {
  location: 'İL - İLÇE - SEMT - MAHALLE',
  project: 'SİTELER',
  poi: 'YAKININDA ARA',
};

const searchGroupOrder = {
  location: 0,
  poi: 1,
  project: 2,
};

const locationSearchProvider = (text, cityId) => {
  if (!text || text.length <= 2) {
    return Promise.resolve([]);
  }

  return search.suggest(text, ['location', 'project', 'poi'], cityId).then(data =>
    Object.keys(data)
      .map((key) => {
        const header = searchHeaders[key] || 'Diğer';
        const locations = data[key].map((loc) => {
          // fix inconsistency in api replies
          if (loc.locality && !loc.locality.push) {
            loc.locality = [loc.locality];
          }

          if (loc.town) {
            if (!loc.town.push) {
              loc.town = [loc.town];
            }
          }

          if (loc.district) {
            if (!loc.district.push) {
              loc.district = [loc.district];
            }
          }

          if (loc.city) {
            if (!loc.city.push) {
              loc.city = [loc.city];
            }
          }

          return loc;
        });

        return { key, header, locations };
      })
      .filter(group => group.locations.length > 0 && !!searchHeaders[group.key])
      .sort((a, b) => {
        const va = searchGroupOrder[a.key];
        const vb = searchGroupOrder[b.key];

        if (va === vb) {
          return 0;
        }

        return va > vb ? 1 : -1;
      })
  );
};

export default locationSearchProvider;
