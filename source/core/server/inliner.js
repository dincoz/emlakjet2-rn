const fs = require('fs');

/**
 * Get stats.
 */
// eslint-disable-next-line
const clientStats = require('../../stats/client.json');

/**
 * Get main script files.
 */
const SCRIPT_FILES = clientStats.entrypoints.client.assets.filter(filename =>
  /\.js$/.test(filename)
);

/**
 * Get main style files.
 */
const STYLE_FILES = clientStats.entrypoints.client.assets.filter(filename =>
  /\.css$/.test(filename)
);

/**
 * Get core of inline scripts as string.
 */
const INLINE_SCRIPT = fs.readFileSync('./build/inline/index.js', 'utf8');

/**
 * Encapsulate with IIFE.
 */
function encapsulate(code) {
  return `(function() {${code}})()`;
}

/**
 * Create inline script body.
 */
export function getInlineScript(extraScriptFiles = [], extraStyleFiles = []) {
  // Create final script script files list.
  const scriptFiles = [...SCRIPT_FILES, ...extraScriptFiles];
  const styleFiles = [...STYLE_FILES, ...extraStyleFiles];

  // Return encapsulated script.
  return encapsulate(
    // Script files to load async.
    `var SCRIPT_FILES = ${JSON.stringify(scriptFiles)};` +
      // Style files to load async.
      `var STYLE_FILES = ${JSON.stringify(styleFiles)};` +
      // Core inline script.
      `${INLINE_SCRIPT}`
  );
}
