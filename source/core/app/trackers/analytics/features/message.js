const actionListeners = {
  messageFormOpenedFromStickyMenu() {
    return {
      eventCategory: 'messaging',
      eventAction: 'message-opened-from-sticky-menu',
    };
  },
  messageFormOpenedFromAgentSection() {
    return {
      eventCategory: 'messaging',
      eventAction: 'message-opened-from-agent-section',
    };
  },
};

export default actionListeners;
