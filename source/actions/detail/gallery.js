import { SLIDE_LISTING_DETAIL_PHOTO, EXTEND_LISTING_PHOTO_GALLERY } from 'symbols/detail/gallery';

export const slideListingDetailPhoto = page => ({
  type: SLIDE_LISTING_DETAIL_PHOTO,
  page,
});

export const extendListingPhotoGallery = page => ({
  type: EXTEND_LISTING_PHOTO_GALLERY,
  page,
});
