/**
 * Configure services.
 * TODO: Make this dynamic somehow.
 */




import { setConfig } from '@emlakjet/services/source';
import appConfig from '/app-config';

setConfig({
  url: {
    api: appConfig.apps.api.hostname,
    php: appConfig.apps.php.hostname,
  },
});

export default () => {};
