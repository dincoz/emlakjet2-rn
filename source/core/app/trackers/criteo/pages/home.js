import { criteo } from 'app-config';

const actionListeners = {
  homepageLoadRouteSuccess() {
    const events = [
      { event: 'setAccount', account: criteo.accountId },
      { event: 'setHashedEmail', email: criteo.email },
      { event: 'setSiteType', type: 'm' },
      { event: 'viewHome' },
    ];

    return events;
  },
};

export default actionListeners;
