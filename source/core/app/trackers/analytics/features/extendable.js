const actionListeners = {
  extendListingDetailDescription() {
    return {
      eventCategory: 'listing-detail-description',
      eventAction: 'description-expanded',
    };
  },
};

export default actionListeners;
