/**
 * Export constants.
 */
export const LOAD_ROUTE = 'LOAD_ROUTE';
export const LOAD_ROUTE_RESOLVED = 'LOAD_ROUTE_RESOLVED';
export const LOAD_ROUTE_SUCCESS = 'LOAD_ROUTE_SUCCESS';
export const LOAD_ROUTE_FAILURE = 'LOAD_ROUTE_FAILURE';
