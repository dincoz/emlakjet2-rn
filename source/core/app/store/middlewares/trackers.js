import trackerFactories from 'core/app/trackers';

import { constantCaseToCamelCase, constantCaseToPascalCase } from '@emlakjet/commons/utils/string';

import { getCurrentRouting } from 'selectors/routing';

import { LOAD_ROUTE_SUCCESS } from 'symbols/routing';

import { APP_INITIALIZED } from 'symbols/initial';

export default (store) => {
  const trackers = trackerFactories.map(tracker => tracker());

  return next => (action) => {
    const state = store.getState();
    const currentRouting = getCurrentRouting(state);
    const isPageView = action.type === LOAD_ROUTE_SUCCESS || action.type === APP_INITIALIZED;

    const actionListenerName = isPageView
      ? currentRouting.pagename + constantCaseToPascalCase(LOAD_ROUTE_SUCCESS)
      : constantCaseToCamelCase(action.type);

    trackers.forEach((tracker) => {
      if (typeof tracker === 'function') {
        tracker(actionListenerName, isPageView, state, action);
      }
    });

    return next(action);
  };
};
