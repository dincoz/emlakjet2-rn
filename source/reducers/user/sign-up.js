import { combineReducers } from 'redux';

/**
 * Get symbols.
 */
import { DO_SIGN_UP, DO_SIGN_UP_SUCCESS, DO_SIGN_UP_FAILURE } from 'symbols/user/sign-up';

/**
 * Create root reducer.
 */
export default combineReducers({
  result: (result = false, action) => {
    if (action.type === DO_SIGN_UP_SUCCESS) {
      return {
        ...action.result,
        error: false,
      };
    }

    if (action.type === DO_SIGN_UP_FAILURE) {
      return {
        error: action.error.response ? action.error.response.status : true,
      };
    }

    // Return as is.
    return result;
  },

  isRegistrationInProgress: (state = false, action) => {
    switch (action.type) {
      case DO_SIGN_UP:
        return {
          state: true,
          error: true,
        };

      case DO_SIGN_UP_SUCCESS:
      case DO_SIGN_UP_FAILURE:
        return false;

      default:
        return state;
    }
  },
});
