import phone from './phone';
import bottomStickyButtons from './bottom-sticky-buttons';
import preparedSearches from './prepared-searches';
import share from './share';
import gallery from './gallery';
import extendable from './extendable';
import map from './map';
import realEstateAgentTag from './real-estate-agent-tag';
import message from './message';
import search from './search';
import favorite from './favorite';
import signin from './signin';
import signup from './signup';
import facebook from './facebook';
import savesearch from './save-search';

const actionListeners = {
  ...phone,
  ...bottomStickyButtons,
  ...preparedSearches,
  ...share,
  ...gallery,
  ...extendable,
  ...map,
  ...realEstateAgentTag,
  ...message,
  ...search,
  ...favorite,
  ...signin,
  ...signup,
  ...facebook,
  ...savesearch,
};

export default actionListeners;
