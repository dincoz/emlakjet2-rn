import { takeLatest, select, put } from 'redux-saga/effects';
import SearchURLMaker from '@emlakjet/commons/helpers/search-url-maker';

/**
 * Import related symbols.
 */
import { DO_ACCOUNT_SEARCH } from 'symbols/search/account';

/**
 * Import routing actions.
 */
import { loadRouteResolved } from 'actions/routing';

/**
 * Import account search actions.
 */
import { replaceAccountSearchCriteria } from 'actions/search/account';

/**
 * Import account search state selectors.
 */
import { getAccountSearchCriteria } from 'selectors/search/account';

/**
 * Watch do account search action.
 */
function* watcher() {
  yield takeLatest(DO_ACCOUNT_SEARCH, function* doAccountSearch({ criteria }) {
    let searchCriteria = criteria;

    // When no criteria passed with action, take account search criteria from store.
    if (!searchCriteria) {
      const state = yield select();

      searchCriteria = getAccountSearchCriteria(state);
    }

    // Replace all account search criteria with active criteria.
    yield put(replaceAccountSearchCriteria(searchCriteria));

    // Push new search URL to the history.
    return yield put(
      loadRouteResolved({
        pathname: new SearchURLMaker(searchCriteria).get(),
        pagename: 'accountDetail',
        parsedURLData: searchCriteria,
      })
    );
  });
}

export default watcher;
