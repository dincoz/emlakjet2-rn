import { DO_SIGN_UP, DO_SIGN_UP_SUCCESS, DO_SIGN_UP_FAILURE, CHANGE_SIGNUP_TAB, CLICK_SIGN_UP_FACEBOOK, DO_SIGN_UP_ERROR } from 'symbols/user/sign-up';

export const doSignUp = data => ({
  type: DO_SIGN_UP,
  data,
});

export const doSignUpFailure = error => ({
  type: DO_SIGN_UP_FAILURE,
  error,
});

export const doSignUpSuccess = result => ({
  type: DO_SIGN_UP_SUCCESS,
  result,
});

export const changeSignupTab = data => ({
  type: CHANGE_SIGNUP_TAB,
  data
});

export const clickSignUpFacebook = () => ({
  type: CLICK_SIGN_UP_FACEBOOK
});

export const doSignUpError = data => ({
  type: DO_SIGN_UP_ERROR,
  data
});
