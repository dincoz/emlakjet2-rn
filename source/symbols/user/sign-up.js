/**
 * Export constants.
 */
export const DO_SIGN_UP = 'DO_SIGN_UP';
export const DO_SIGN_UP_SUCCESS = 'DO_SIGN_UP_SUCCESS';
export const DO_SIGN_UP_FAILURE = 'DO_SIGN_UP_FAILURE';
export const CHANGE_SIGNUP_TAB = 'CHANGE_SIGNUP_TAB';
export const CLICK_SIGN_UP_FACEBOOK = 'CLICK_SIGN_UP_FACEBOOK';
export const DO_SIGN_UP_ERROR = 'DO_SIGN_UP_ERROR';
