import {
  LOAD_ROUTE,
  LOAD_ROUTE_RESOLVED,
  LOAD_ROUTE_SUCCESS,
  LOAD_ROUTE_FAILURE,
} from 'symbols/routing';

export const loadRoute = ({ action, pathname }) => ({
  type: LOAD_ROUTE,
  action,
  pathname,
});

export const loadRouteResolved = ({ pathname, pagename, parsedURLData }) => ({
  type: LOAD_ROUTE_RESOLVED,
  pathname,
  pagename,
  parsedURLData,
});

export const loadRouteSuccess = (
  {
    pathname, pagename, parsedURLData, initialData, seoData
  } = {}
) => ({
  type: LOAD_ROUTE_SUCCESS,
  pathname,
  pagename,
  parsedURLData,
  initialData,
  seoData,
});

export const loadRouteFailure = ({ error }) => ({
  type: LOAD_ROUTE_FAILURE,
  error,
});
