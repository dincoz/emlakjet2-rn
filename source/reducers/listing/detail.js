import { fromJS } from 'immutable';

import normalizeListing from '@emlakjet/normalizers/listing';

/**
 * Get symbols.
 */
import { FETCH_LISTING_DETAIL_SUCCESS } from 'symbols/listing-detail';

/**
 * Create root reducer.
 */
export default (state = null, action) => {
  switch (action.type) {
    case FETCH_LISTING_DETAIL_SUCCESS:

    // Create new state from new result object.
      return fromJS(normalizeListing(action.detail));

    default:
    // Return as is.
      return state;
  }
};
