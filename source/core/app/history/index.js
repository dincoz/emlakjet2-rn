import createBrowserHistory from 'history/createBrowserHistory';
import createMemoryHistory from 'history/createMemoryHistory';

/**
 * Determine correct history creator
 */
const IS_CLIENT = false;
export default (IS_CLIENT ? createBrowserHistory : createMemoryHistory);
