import { take, put, race } from 'redux-saga/effects';

import { fetchAccountListings } from 'actions/search/account';

import {
  FETCH_ACCOUNT_LISTINGS_SUCCESS,
  FETCH_ACCOUNT_LISTINGS_FAILURE,
} from 'symbols/search/account';

export default function* accountDetail(parsedURLData) {
  /**
   * Dispatch initial-data fetcher action.
   */
  yield put(fetchAccountListings(parsedURLData));

  /**
   * Wait for success or failure.
   */
  const { success, failure } = yield race({
    success: take(FETCH_ACCOUNT_LISTINGS_SUCCESS),
    failure: take(FETCH_ACCOUNT_LISTINGS_FAILURE),
  });

  /**
   * On account-detail fetch, success has to have result.
   */
  if (success && success.result) {
    return [success.result, null];
  }

  /**
   * Return failure.
   */
  return [null, failure];
}
