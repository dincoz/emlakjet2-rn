import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEventPattern';

export const createObservableStore = store =>
  Observable.fromEventPattern(
    handler => store.subscribe(handler),
    (handler, unsubscribe) => unsubscribe(),
    () => store.getState()
  );
