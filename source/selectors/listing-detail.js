import { createSelector } from 'reselect';

const getListingDetailImmutable = state => state.listing.detail;

export const getListingDetail = createSelector(
  getListingDetailImmutable,
  detail => (detail && detail.toJS ? detail.toJS() : detail)
);
