import { clone, last, pick } from 'lodash';

import { getRoutingHistory, getCurrentRouting } from 'selectors/routing';

export default {
  loadRouteSuccess(state, storageState) {
    const currentRoutingHistory = getRoutingHistory(state);
    const routingHistory = storageState.routingHistory || {};

    let expiryDate = Date.now();
    const isExpired = expiryDate > routingHistory.expiryDate || !routingHistory.expiryDate;

    expiryDate = isExpired ? (expiryDate + 60) * 60 * 1000 : routingHistory.expiryDate;

    let lastHistoryItem = currentRoutingHistory.length && clone(last(currentRoutingHistory));

    if (!lastHistoryItem) {
      const currentRouting = getCurrentRouting(state);

      lastHistoryItem = pick(currentRouting, ['pagename', 'pathname']);
    }

    lastHistoryItem.passive = true;
    delete lastHistoryItem.inProgress;

    const data = [].concat(isExpired ? [] : routingHistory.data || [], lastHistoryItem || []);

    return {
      routingHistory: {
        path: 'routing.history',
        data,
        expiryDate,
      },
    };
  },
};
