import { combineReducers } from 'redux';

/**
 * Get reducers.
 */
import estate from './estate';
import account from './account';

/**
 * Create searches reducer.
 */
export default combineReducers({
  estate,
  account,
});
