// eslint-disable-next-line
//import criteo from '@emlakjet/criteo-fork';

import pages from './pages';
import features from './features';

const actionListeners = {
  ...pages,
  ...features,
};

const eventSender = (actionListenerName, isPageView, state, action) => {
  if (typeof window === 'undefined' || !window.criteo_q) {
    return;
  }

  const actionListener = actionListeners[actionListenerName];

  if (!isPageView && !actionListener) {
    return;
  }

  const eventParameters = actionListener ? actionListener(state, action) : [];

  if (eventParameters.length > 0) {
    window.criteo_q.push(...eventParameters);
  }
};

export default () => eventSender;
