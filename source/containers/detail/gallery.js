import { slideListingDetailPhoto, extendListingPhotoGallery } from 'actions/detail/gallery';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({});

const mapDispatchToProps = {
  slideListingDetailPhoto,
  extendListingPhotoGallery,
};

export default [mapStateToProps, mapDispatchToProps];
