import { VIEW_MARKER_SUMMARY } from 'symbols/listing/map';

export const viewMarkerSummary = listing => ({
  type: VIEW_MARKER_SUMMARY,
  listing,
});
