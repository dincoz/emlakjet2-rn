/**
 * Import account search actions.
 */
import {
  fetchAccountListings,
  fetchAccountShowcase,
  setAccountSearchCriteria,
  replaceAccountSearchCriteria,
  doAccountSearch,
} from 'actions/search/account';

/**
 * Import account search selectors.
 */
import { getAccountSearchResult, getAccountSearchCriteria } from 'selectors/search/account';

/**
 * Map selected state objects to the connected component's props.
 */
// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => ({
  accountSearchResult: getAccountSearchResult(state),
  accountSearchCriteria: getAccountSearchCriteria(state),
});

/**
 * Map action list to the connected component's props by dispatching.
 */
const mapDispatchToProps = {
  fetchAccountListings,
  fetchAccountShowcase,
  setAccountSearchCriteria,
  replaceAccountSearchCriteria,
  doAccountSearch,
};

// Export mappers.
export default [mapStateToProps, mapDispatchToProps];
